#Script to collect the list of stranded files from the last 24 hours by excluding the files stranded in as2inbox,subscriptions,sent,outbox and ZERO byte files
#!/bin/sh
TIME=`date '+%H%M%S'`
DATE=`date '+%m%d%Y'`
SCRIPT_WORK_DIRECTORY="/amex/data/SupportScripts/StrandedFilesScript"
. $SCRIPT_WORK_DIRECTORY/priority_file_details.txt
LOG_PATH="/amex/data/SupportScripts/StrandedFilesScript/logs"
STRANDEDFILES_LIST=$LOG_PATH/Strandedfile_list$DATE_$TIME
MASTER_LIST=$LOG_PATH/MASTER_LIST_$DATE_$TIME
SETTLEMENTFILES_LIST=$SCRIPT_WORK_DIRECTORY/SETTLEMENTFILES_LIST
SCRIPT_LOG_FILE="$LOG_PATH/Strandedfiles.D`date '+%m%d%Y'`.T`date '+%H%M%S'`.log"
SEARCH_PATH="/amex/data/users"
LIST1="$SCRIPT_WORK_DIRECTORY/GNS_STRANDEDFILES_$DATE_$TIME"
LIST2="$SCRIPT_WORK_DIRECTORY/SETTLEMENT_STRANDEDFILES_$DATE_$TIME"
LIST3="$SCRIPT_WORK_DIRECTORY/EMM_STRANDEDFILES_$DATE_$TIME"
LIST4="$SCRIPT_WORK_DIRECTORY/Other_STRANDEDFILES_$DATE_$TIME"
LIST5="$SCRIPT_WORK_DIRECTORY/JAPA_STRANDEDFILES_$DATE_$TIME"
#sed ' ' $LIST2 > $LIST2
Num_Strandedfiles=0
Num_GNS_fies=0
Num_SETTLEMENT_files=0
Num_EMM_files=0
Num_JAPA_files=0
Other_files=0

#Max Log retention period under $LOG_PATH directory
LOG_MAX_TTL=30

if [ -e $SCRIPT_WORK_DIRECTORY/`basename $0`_FLAG ]
then
        echo "Error: An instance of this script is already running!"
        exit 1
else
        echo "*********************************************************************************************" >> $SCRIPT_LOG_FILE
        echo `date '+%m/%d/%Y'` `date '+%H:%M:%S'` "Script started" >> $SCRIPT_LOG_FILE
        touch $SCRIPT_WORK_DIRECTORY/`basename $0`_FLAG
                echo -e "\e[1;31m"For better performance execute the script in NFS server"\e[0m" | tee -a $SCRIPT_LOG_FILE
                echo -n "How many hours older files you want to search : "
                read HOURS_TO_SEARCH
                X=`echo $HOURS_TO_SEARCH | tr -dc  '[:digit:]' `
                if [ "$X" == "$HOURS_TO_SEARCH"  -a  "$HOURS_TO_SEARCH" != "" ] ; then
                        MIN_TO_SEARCH=`expr $HOURS_TO_SEARCH \* 60`
                        echo "MIN_TO_SEARCH= $MIN_TO_SEARCH" >> $SCRIPT_LOG_FILE
                else
                        echo -e "Please enter a valid numeric number"
                        rm $SCRIPT_WORK_DIRECTORY/`basename $0`_FLAG
                        exit 0;
                fi
        FROM_TIME=`date -d ''$MIN_TO_SEARCH' min ago' '+%Y/%m/%d %H:%M'`
        TO_TIME=`date -d '10 min ago' '+%Y/%m/%d %H:%M'`
        echo "Started searching for the stranded files from $FROM_TIME to $TO_TIME " | tee -a $SCRIPT_LOG_FILE
        echo "Script execution statred at `date '+%m/%d/%Y'` `date '+%H:%M:%S'`" | tee -a $SCRIPT_LOG_FILE
        echo "Log files stored at: $LOG_PATH"
        echo "Please wait till the script execution complete..." | tee -a $SCRIPT_LOG_FILE
        #To extract the files that are stranded in the users home and its subdirectories
        find $SEARCH_PATH -type f \( -mmin +10 -a -mmin -$MIN_TO_SEARCH \) \( ! -regex '.*/\..*' \) -size +0 -print 2>/dev/null | egrep -v 'subscriptions|sent|outbox' > $STRANDEDFILES_LIST
        cp $STRANDEDFILES_LIST $MASTER_LIST
        if [ -s $STRANDEDFILES_LIST ];
        then
                Num_Strandedfiles=`wc -l $STRANDEDFILES_LIST | cut -d ' ' -f1`
                echo `date '+%m/%d/%Y'` `date '+%H:%M:%S'` "Total $Num_Strandedfiles files got stranded in SFT" >> $SCRIPT_LOG_FILE
                echo `date '+%m/%d/%Y'` `date '+%H:%M:%S'` "Searching for GNS files in $STRANDEDFILES_LIST" >> $SCRIPT_LOG_FILE
                if [ ` grep -c $GNS_PATTEREN $STRANDEDFILES_LIST ` -gt 0 ]
                then
                     grep $GNS_PATTEREN $STRANDEDFILES_LIST > $LIST1
                     echo `date '+%m/%d/%Y'` `date '+%H:%M:%S'` "Stranded GNS files are in $LIST1" >> $SCRIPT_LOG_FILE
                     sed -i '/'$GNS_PATTEREN'/d' $STRANDEDFILES_LIST
                else
                    echo `date '+%m/%d/%Y'` `date '+%H:%M:%S'` "No GNS files in $STRANDEDFILES_LIST" >> $SCRIPT_LOG_FILE
                    test -e $LIST1 && rm $LIST1
                fi
                echo `date '+%m/%d/%Y'` `date '+%H:%M:%S'` "Searching for EMM files in $STRANDEDFILES_LIST" >> $SCRIPT_LOG_FILE
                if [ ` egrep -c '('$EMM_USER1' |'$EMM_USER2'|'$EMM_USER3'|'$EMM_USER4'|'$EMM_USER5'|'$EMM_USER6'|'$EMM_USER7'|'$EMM_USER8')' $STRANDEDFILES_LIST` -gt 0 ]
                then
                        egrep '('$EMM_USER1'|'$EMM_USER2'|'$EMM_USER3'|'$EMM_USER4'|'$EMM_USER5'|'$EMM_USER6'|'$EMM_USER7'|'$EMM_USER8')' $STRANDEDFILES_LIST > $LIST3
                        echo `date '+%m/%d/%Y'` `date '+%H:%M:%S'` "Stranded EMM files are in $LIST3" >> $SCRIPT_LOG_FILE
                        sed -i '/'$EMM_USER1'/d;/'$EMM_USER2'/d;/'$EMM_USER3'/d;/'$EMM_USER4'/d;/'$EMM_USER5'/d;/'$EMM_USER6'/d;/'$EMM_USER7'/d;/'$EMM_USER8'/d' $STRANDEDFILES_LIST
                        else
                            echo `date '+%m/%d/%Y'` `date '+%H:%M:%S'` "No EMM files in $STRANDEDFILES_LIST" >> $SCRIPT_LOG_FILE
                            test -e $LIST3 && rm $LIST3
                fi
                       
                echo `date '+%m/%d/%Y'` `date '+%H:%M:%S'` "Searching for JAPA files in $STRANDEDFILES_LIST" >> $SCRIPT_LOG_FILE
                if [ ` egrep -c '('$JAPA_USER1'|'$JAPA_USER2'|'$JAPA_USER3')' $STRANDEDFILES_LIST` -gt 0 ]
                then
                        egrep '('$JAPA_USER1'|'$JAPA_USER2'|'$JAPA_USER3')' $STRANDEDFILES_LIST > $LIST5
                        echo `date '+%m/%d/%Y'` `date '+%H:%M:%S'` "Stranded JAPA files are in $LIST5" >> $SCRIPT_LOG_FILE
                        sed -i '/'$JAPA_USER1'/d;/'$JAPA_USER2'/d;/'$JAPA_USER3'/d' $STRANDEDFILES_LIST
                        else
                            echo `date '+%m/%d/%Y'` `date '+%H:%M:%S'` "No JAPA files in $STRANDEDFILES_LIST" >> $SCRIPT_LOG_FILE
                            test -e $LIST5 && rm $LIST5
                fi				

                echo `date '+%m/%d/%Y'` `date '+%H:%M:%S'` "Searching for Submission SETTLEMENT files in $STRANDEDFILES_LIST" >> $SCRIPT_LOG_FILE
                if [ -e $SETTLEMENTFILES_LIST ]
                then
                    for file in `cat $STRANDEDFILES_LIST`
                    do
                        SEARCH_FILENAME=` basename $file `
                        #cho "file name =$SEARCH_FILENAME"
                        grep -q $SEARCH_FILENAME $SETTLEMENTFILES_LIST
                        if [ $? -eq 0 ]
                        then
                                echo $file >> $LIST2
                                sed -i '/'$SEARCH_FILENAME'/d' $STRANDEDFILES_LIST
                        fi
                    done
                    if [ -s $LIST2 ]
                    then
                          echo `date '+%m/%d/%Y'` `date '+%H:%M:%S'` "Stranded Submission SETTLEMENT files are in $LIST2" >> $SCRIPT_LOG_FILE
                    else
                          echo `date '+%m/%d/%Y'` `date '+%H:%M:%S'` "No Submission SETTLEMENT files in $STRANDEDFILES_LIST" >> $SCRIPT_LOG_FILE
                          test -e $LIST2 && rm $LIST2
                    fi
                else
                echo `date '+%m/%d/%Y'` `date '+%H:%M:%S'` "$SETTLEMENTFILES_LIST is not available in the $SCRIPT_WORK_DIRECTORY location.Please check." >> $SCRIPT_LOG_FILE
                fi
                mv $STRANDEDFILES_LIST $LIST4
                if  [ $? -eq 0 ]
                then
                    if [ -s $LIST4 ]
                    then
                        echo `date '+%m/%d/%Y'` `date '+%H:%M:%S'` "All the remaining stranded files are in $LIST4" >> $SCRIPT_LOG_FILE
                    else
                        echo `date '+%m/%d/%Y'` `date '+%H:%M:%S'` "No stranded files for other users" >> $SCRIPT_LOG_FILE
                        #sed ' ' $LIST4 > $LIST4
                        test -e $LIST4 && rm $LIST4
                    fi
                fi
                                echo "------------------------------------------" | tee -a $SCRIPT_LOG_FILE
                                echo "Total number of stranded files    = $Num_Strandedfiles" | tee -a $SCRIPT_LOG_FILE
                                if [ -e $LIST1 ]
                                then
                                Num_GNS_fies=`wc -l $LIST1 | cut -d ' ' -f1`
                                echo "Total GNS stranded files          = $Num_GNS_fies" | tee -a $SCRIPT_LOG_FILE
                                fi
                                if [ -s $LIST2 ]
                                then
                                Num_SETTLEMENT_files=`wc -l $LIST2 | cut -d ' ' -f1`
                                echo "Total settlement stranded files   = $Num_SETTLEMENT_files" | tee -a $SCRIPT_LOG_FILE
                                fi
                                if [ -e $LIST3 ]
                                then
                                Num_EMM_files=`wc -l $LIST3 | cut -d ' ' -f1`
                                echo "Total EMM stranded files          = $Num_EMM_files" | tee -a $SCRIPT_LOG_FILE
                                fi
                                if [ -e $LIST5 ]
				then
                                Num_JAPA_files=`wc -l $LIST5 | cut -d ' ' -f1`
                                echo "Total JAPA stranded files          = $Num_JAPA_files" | tee -a $SCRIPT_LOG_FILE
                                fi
                                if [ -e $LIST4 ]
                                then
                                Other_files=`wc -l $LIST4 | cut -d ' ' -f1`
                                echo  " Stranded files(Excluding GNS/EMM/SETTLEMNT/JAPA)  = $Other_files" | tee -a $SCRIPT_LOG_FILE
                                fi
                                echo "-------------------------------------------" | tee -a $SCRIPT_LOG_FILE
        else
                echo `date '+%m/%d/%Y'` `date '+%H:%M:%S'` " UPDATE:No Strandedfiles " | tee -a $SCRIPT_LOG_FILE
        fi
        echo "Script execution completed `date '+%m/%d/%Y'` `date '+%H:%M:%S'` . Refer the logs and make sure all strandrad file(s) are taken care. Thank you" | tee -a $SCRIPT_LOG_FILE
fi

    #########
    #Purge old logs in ${LOG_PATH} that are 30 days older 
    #########
    echo -e "\n" >> $SCRIPT_LOG_FILE
    echo "---------------------------------Deleting old log files from $SCRIPT_LOG_FILE directory--------------" >> $SCRIPT_LOG_FILE
    if [ -d $LOG_PATH ]
    then
       echo "Searching for old log files under $LOG_PATH" >> $SCRIPT_LOG_FILE
       for i in `find ${LOG_PATH} -type f -mtime +$LOG_MAX_TTL 2>/dev/null`
       do
           echo "Cleaning up Old Logs morethan 30 days" $i >> $SCRIPT_LOG_FILE
           rm -f $i
       done
    else
       echo "$LOG_PATH Directory not found in $SYSTEM_NAME." >> $SCRIPT_LOG_FILE
    fi
    echo "----------------------------------------------------------------------------------------------------------------" >> $SCRIPT_LOG_FILE


echo "*********************************************************************************************" >> $SCRIPT_LOG_FILE
rm $SCRIPT_WORK_DIRECTORY/`basename $0`_FLAG
