#!/bin/sh
ALERT_DATE=`date '+%m/%d/%Y'`
ALERT_TIME=`date '+%H:%M:%S'`
HOST_NAME="`hostname -s`"
SCRIPT_WORKING_DIRECTORY=/amex/data/SupportScripts/ 
SCRIPT_LOG_DIR=/amex/data/SupportScripts/validation_logs/
#SCRIPT_LOG_FILE=/home/E3mgopala/validationcheck.D`date '+%m%d%Y'`.T`date '+%H:%M:%S'`.sh
SCRIPT_LOG_FILE=$SCRIPT_LOG_DIR/`hostname -s`.validationcheck.D`date '+%m%d%Y'`.T`date '+%H%M%S'`.out
#EMAIL_OUT_FILE=$SCRIPT_LOG_DIR/`hostname -s`.validationcheckEMail.D`date '+%m%d%Y'`.T`date '+%H%M%S'`
SFT_TRACE_LOG_FILE=/logs/sft/SFTTrace.log
NET_STAT_OUT_FILE=$SCRIPT_LOG_DIR/`hostname -s`.netstat_output`date '+%m%d%Y'`.T`date '+%H%M%S'`
MQ_FILE=/logs/mqm/connection-count.log
Folder_Monitor=/logs/sft/foldermonitor_alerts.log
cron_alerts=/logs/sft/cron_alerts.log
xferlogpath=/amex/st/var/logs
CDlogspath=/logs/sft/CDtest.log

#Max Log retention period under $SCRIPT_LOG_DIR directory
LOG_MAX_TTL=5
temp=$SCRIPT_WORKING_DIRECTORY/temp_`hostname -s`

#Error Messages
SERVER_OFFLINE_ERR_MSG="CRITICAL ERROR:: Server goes offline: "
DB2G_ERR_MSG="CRITICAL ERROR:: NOT ABLE TO CONNECT IPCWDB2G DATABASE. PLESAE CHECK THE CONNECTIVITY."
NDM_ERR_MSG="CRITICAL ERROR:: NOT ABLE TO CONNECT IPCW NODE NDM SERVER. PLESAE CHECK THE CONNECTIVITY."
CD_ERR_MSG="CRITICAL ERROR:: Connect Direct service is not running on "
SFT_CONTAINER_ERR_MSG="CRITICAL ERROR:: SFTContainer is not running on "

#Trace/Log Messages
SFT_TRACE_MSG1="File Mailbox Step - Placing file to  transmitter's outbox - /amex/data/users/00000000090/outbox/"
#SFT_TRACE_MSG2=""
MSG_STRING="$ALERT_DATE $ALERT_TIME $HOST_NAME : Validation Script executed. Please find the test results below,"

	echo "$MSG_STRING" >> $EMAIL_OUT_FILE
        echo -e "\n" | tee -a $EMAIL_OUT_FILE
	echo "$MSG_STRING" >> $SCRIPT_LOG_FILE
	echo -e "\n" >> $SCRIPT_LOG_FILE	

	# Validating cluster status
	echo "------------------------------------Cluster status of all PHX BE servers------------------------------------" >> $SCRIPT_LOG_FILE
	echo "`cat  /amex/st/var/tmp/cluster_state | tr '<>' '\n' | grep Member | tr '/' '\n'`" >> $SCRIPT_LOG_FILE 
        echo "`cat  /amex/st/var/tmp/cluster_state | tr '<>' '\n' | grep Member | tr '/' '\n'`" >> $EMAIL_OUT_FILE 
        cat  /amex/st/var/tmp/cluster_state | tr '<>' '\n' | grep Member | tr '/' '\n' | grep offline > /dev/null
        if [ $? -eq 0 ]
        then
 	   echo -e "\n" | tee -a $EMAIL_OUT_FILE
	   echo -e "\e[1;31m"-------------------------Please check the cluster state in Admin GUI below listed server"("s")" is offline-------"\e[0m" | tee -a $EMAIL_OUT_FILE
           echo -e "\e[1;31m"$SERVER_OFFLINE_ERR_MSG `cat /amex/st/var/tmp/cluster_state | tr '</>' '\n' | grep Member | grep offline`"\e[0m" | tee -a $EMAIL_OUT_FILE
	   echo -e "\n" | tee -a $EMAIL_OUT_FILE
        fi
	
	echo -e "\n" >> $SCRIPT_LOG_FILE 

	# Validating DB2G Connectivity
	echo "------------------------------------DB CONNECTIVITY RESULT--------------------------------------------------" >> $SCRIPT_LOG_FILE
	nohup telnet ipcwdb2g.ipc.us.aexp.com 7910   >>  $SCRIPT_LOG_FILE 

        grep -1 "ipcwdb2g.ipc.us.aexp.com" $SCRIPT_LOG_FILE | grep "Connection refused" > /dev/null
	if [ $? -eq 0 ]
        then
	   echo -e "\e[1;31m"$DB2G_ERR_MSG"\e[0m" | tee -a $EMAIL_OUT_FILE
           echo -e "\n" | tee -a $EMAIL_OUT_FILE
	fi

	# Validating NDM Connectivity
	echo -e "\n" >> $SCRIPT_LOG_FILE
	echo "------------------------------------IPCW NODE NDM (Network Data Mover) CONNECTIVITY RESULT------------------" >> $SCRIPT_LOG_FILE
	nohup telnet 148.171.103.24 1364  >>  $SCRIPT_LOG_FILE

        grep -1 "148.171.103.24" $SCRIPT_LOG_FILE | grep "Connection refused" > /dev/null
        if [ $? -eq 0 ]
        then
           echo -e "\e[1;31m"$NDM_ERR_MSG"\e[0m" | tee -a $EMAIL_OUT_FILE
           echo -e "\n" | tee -a $EMAIL_OUT_FILE
        fi

	echo -e "\n" >> $SCRIPT_LOG_FILE

	# ST services validation
	echo -e "\e[1;34m"------------------------------------Status of all services on `hostname -s` server---------------------------------"\e[0m" | tee -a $EMAIL_OUT_FILE
	netstat -an | grep LISTEN > $NET_STAT_OUT_FILE
	for port in `cat $SCRIPT_WORKING_DIRECTORY/portnum | cut -d' ' -f1`
	do
		#port="echo "$service" | awk '{print $1}'"
		#service_name="echo "$service" | awk '{print $2}'"
		grep -q ":$port" $NET_STAT_OUT_FILE
		if [ $? -eq 0 ]
		then
                  echo " `grep $port $SCRIPT_WORKING_DIRECTORY/portnum | awk '{print $2}'` service is up and running on `hostname`" | tee -a $EMAIL_OUT_FILE
                  echo " `grep $port $SCRIPT_WORKING_DIRECTORY/portnum | awk '{print $2}'` service is up and running on `hostname`" >> $SCRIPT_LOG_FILE
		else 
                    echo -e  "\e[1;31m "CRITICAL ERROR:: `grep $port $SCRIPT_WORKING_DIRECTORY/portnum | 
                      awk '{print $2}'` service is not running on `hostname`" \e[0m" | tee -a $EMAIL_OUT_FILE
		fi
	done
	
	# Connect Direct Service Validation
	if ps ax | grep -v grep | grep cdpmgr > /dev/null
	then
	    echo " Connect Direct service is up and running on `hostname`" >> $SCRIPT_LOG_FILE
	else
	    echo -e "\e[1;31m" $CD_ERR_MSG `hostname`"\e[0m" >> $SCRIPT_LOG_FILE
	fi

	# SFT Container Validation
	if ps ax | grep -v grep | grep SFTC > /dev/null
	then
	    echo "SFTContainer is up and running on `hostname`" | tee -a $EMAIL_OUT_FILE
            echo "SFTContainer is up and running on `hostname`" >> $SCRIPT_LOG_FILE
	else
	    echo -e "\e[1;31m" $SFT_CONTAINER_ERR_MSG `hostname`"\e[0m" | tee -a $EMAIL_OUT_FILE
            echo -e "\e[1;31m" $SFT_CONTAINER_ERR_MSG `hostname`"\e[0m" >> $SCRIPT_LOG_FILE
	fi

    # Validating NFS/Disk space usage
    df | grep "[80][0-99]%" > /dev/null
    if [ $? -eq 0 ]
    then
       echo -e "\n" >>  $SCRIPT_LOG_FILE
       echo -e  "\e[1;31m"------------------------------------ALERT HIGH DISK SPACE USAGE REPORT------------------------------------"\e[0m" | tee -a $EMAIL_OUT_FILE
       df | grep "[80][0-99]%" | awk '{print "CRITICAL ERROR::"  $5,"has exceeded 80%"}' | tee -a $EMAIL_OUT_FILE
    fi
  
    echo -e "\n" >> $SCRIPT_LOG_FILE
    
    echo "------------------------------------List of all script processes which are currently running on `hostname -a`.----" >> $SCRIPT_LOG_FILE
    ps -acu root | grep ".sh" | grep -v ssh | grep -v pd | grep -v bash | grep -v pts >> $SCRIPT_LOG_FILE
    
    echo -e "\n" | tee -a $EMAIL_OUT_FILE

    echo -e "\e[1;34m"------------------------------------Latest inbound file transfer through `hostname -s`----------------------------"\e[0m" | tee -a $EMAIL_OUT_FILE
    grep -B 1 "Tumbleweed Incoming Start Process Completed" $SFT_TRACE_LOG_FILE | tail -2 >> $SCRIPT_LOG_FILE    
    grep "TumbleweedIncomingStart" $SCRIPT_LOG_FILE > /dev/null
    if [ $? -eq 0 ]
    then
       LATEST_TW_INBOUND=$(grep "TumbleweedIncomingStart" $SCRIPT_LOG_FILE | cut -d ' ' -f2 | cut -d '.' -f1)
       echo "LATEST TUMBLEWEED INBOUND FILE THROUGH `hostname -s` : $LATEST_TW_INBOUND MST" | tee -a $EMAIL_OUT_FILE
       echo "LATEST TUMBLEWEED INBOUND FILE THROUGH `hostname -s` : $LATEST_TW_INBOUND MST" >> $SCRIPT_LOG_FILE
     else
       echo -e "\e[1;31m"THERE IS NO SUCCESSFUL TUMBLEWEED INBOUND FILE THROUGH `hostname` FOR CURRENT HOUR"\e[0m" | tee -a $EMAIL_OUT_FILE
       echo -e "\e[1;31m"THERE IS NO SUCCESSFUL TUMBLEWEED INBOUND FILE THROUGH `hostname` FOR CURRENT HOUR"\e[0m" >> $SCRIPT_LOG_FILE
    fi
    #echo "*********The latest outbound file transfer  through `hostname -s`(Only in Primary server)**********" | tee -a $SCRIPT_LOG_FILE
    #grep -A 1 "deleteControlFile" /logs/sft/SFTTrace.log | tail -3 | tee -a $SCRIPT_LOG_FILE | tee -a $SCRIPT_LOG_FILE
    #echo "-----------------------------------------------------------------"

    echo -e "\n" >> $SCRIPT_LOG_FILE

    #echo "------------------------------------The latest Connect Direct inbound file transfer through `hostname -s`---------" >> $SCRIPT_LOG_FILE
    grep -B 1 "using ConnectDirect" $SFT_TRACE_LOG_FILE | tail -2 >> $SCRIPT_LOG_FILE
    grep "ConnectDirectIncomingEndEvent" $SCRIPT_LOG_FILE > /dev/null
    if [ $? -eq 0 ]
    then
       LATEST_CD_INBOUND=$(grep "ConnectDirectIncomingEndEvent" $SCRIPT_LOG_FILE | cut -d ' ' -f2 | cut -d '.' -f1) 
       echo "LATEST CONNECT DIRECT INBOUND FILE THROUGH `hostname -s` : $LATEST_CD_INBOUND MST" | tee -a $EMAIL_OUT_FILE
       echo "LATEST CONNECT DIRECT INBOUND FILE THROUGH `hostname -s` : $LATEST_CD_INBOUND MST" >> $SCRIPT_LOG_FILE
       echo -e "\n" | tee -a $EMAIL_OUT_FILE
     else
       echo -e "\e[1;31m"THERE IS NO SUCCESSFUL CONNECT DIRECT INBOUND FILE THROUGH `hostname` FOR CURRENT HOUR"\e[0m" | tee -a $EMAIL_OUT_FILE
       echo -e "\e[1;31m"THERE IS NO SUCCESSFUL CONNECT DIRECT INBOUND FILE THROUGH `hostname` FOR CURRENT HOUR"\e[0m" >> $SCRIPT_LOG_FILE
    fi

    echo -e "\n" >> $SCRIPT_LOG_FILE

    echo -e "\e[1;34m"------------------------------------Test file transfer using CDIncoming End script-----------------------------"\e[0m" | tee -a $SCRIPT_LOG_FILE
    FILE_NAME=DAN_FILE_D`date '+%m%d%YY'`_T`date '+%H%M%S'`
    touch /amex/data/users/00000000045/$FILE_NAME
    if [ `whoami` != root ] ; then
       sudo -u tumbleweed /amex/st/brules/local/agents/inprocess/scripts/CDIncomingEnd.sh /amex/data/users/00000000045/$FILE_NAME TWTEST01
    else
       /amex/st/brules/local/agents/inprocess/scripts/CDIncomingEnd.sh /amex/data/users/00000000045/$FILE_NAME TWTEST01
    fi
    echo "File $FILE_NAME has been uploaded from `hostname`" >> $SCRIPT_LOG_FILE
    echo "Logs:" >> $SCRIPT_LOG_FILE
    grep -B 1 "RECEIVED FILE: Username TWTEST01 sent $FILE_NAME" $SFT_TRACE_LOG_FILE >> $SCRIPT_LOG_FILE
    sleep 5
    
    #paste -d/ $SFT_TRACE_MSG1 $FILE_NAME > $SFT_TRACE_MSG2
    grep "File Mailbox Step - Placing file to  transmitter's outbox - /amex/data/users/00000000090/outbox/$FILE_NAME" $SFT_TRACE_LOG_FILE >> $SCRIPT_LOG_FILE

    echo -e "\n" | tee -a $EMAIL_OUT_FILE

    #echo -e "\e[1;34m"------------------------------------Internet VIP test scheduler file upload check--------------------------------"\e[0m" | tee -a $EMAIL_OUT_FILE
    #FTPS_COUNT=`find /amex/data/repository/0000023/00000232484/ -type f -a -mmin -10 | wc -l `
    #HTTPS_COUNT=`find /amex/data/repository/0000023/00000232396/ -type f -a -mmin -10 | wc -l `
    #SSH_COUNT=`find /amex/data/repository/0000023/00000232397/ -type f -a -mmin -10 | wc -l `
    #if [ $FTPS_COUNT -eq 0 ]
    #then
     #  echo "There were no files received through FTPS protocol. Please validate FTPS services." | tee -a $EMAIL_OUT_FILE
    #else
     #  echo "No issue with FTPS scehduler upload."  | tee -a $EMAIL_OUT_FILE
    #fi
    
    #if [ $HTTPS_COUNT -eq 0 ]
    #then
     #  echo "There were no files received through HTTPS protocol. Please validate HTTPS services." | tee -a $EMAIL_OUT_FILE
    #else
     #  echo "No issue with HTTPS scehduler upload."  | tee -a $EMAIL_OUT_FILE
    #fi
    
    #if [ $SSH_COUNT -eq 0 ]
   # then
     #  echo "There were no files received through SSH protocol. Please validate SSH services." | tee -a $EMAIL_OUT_FILE
    #else
     #  echo "No issue with SSH scehduler upload."  | tee -a $EMAIL_OUT_FILE
    #fi
     

    echo "------------------------------------ERRORS FROM LOG FILE-------------------------------------------------------" >> $SCRIPT_LOG_FILE
    grep -q -B 2 "terminated unexpectedly" /var/mqm/errors/AMQERR* | grep "`date '+%m/%d/%Y'`"
	if [ $? -eq 0 ]
	then
                echo -e "\n" | tee -a $EMAIL_OUT_FILE
		echo -e "\e[1;31m"CRITICAL ERROR::MQ error in /var/mqm/errors/AMQERR* log file"\e[0m" | tee -a $EMAIL_OUT_FILE
		grep -B 2 "terminated unexpectedly" /var/mqm/errors/AMQERR* | grep "`date '+%m/%d/%Y'`" | tee -a $EMAIL_OUT_FILE
		#grep -B 2 "An internal WebSphere MQ error has occurred" /var/mqm/errors/AMQERR* | grep "`date '+%m/%d/%Y'`" | tee -a $SCRIPT_LOG_FILE
	else
		echo "There are no MQ errors in /var/mqm/errors/AMQERR* log file" >> $SCRIPT_LOG_FILE
	fi

	tail -10 /logs/sft/Script_logs/cluster_check.`hostname`.`date '+%m%d%Y'`.log | grep -v "PermissionsCheckAgent" | grep -q "Critical error"
	if [ $? -eq 0 ]
	then
                echo -e "\n" | tee -a $EMAIL_OUT_FILE
		echo -e "\e[1;31m"CRITICAL ERROR:: Errors in tm.log file"\e[0m" | tee -a $EMAIL_OUT_FILE
		tail -10 /logs/sft/Script_logs/cluster_check.`hostname`.`date '+%m%d%Y'`.log | grep -v "PermissionsCheckAgent" | grep "Critical error" | tee -a $EMAIL_OUT_FILE
	else
		echo "There are no errors in tm.log file" >> $SCRIPT_LOG_FILE
	fi

    tail -10 /logs/sft/Script_logs/DB_error_check.`hostname`.`date '+%m%d%Y'`.log | grep -q "Critical error"
    if [ $? -eq 0 ]
    then
        echo -e "\n" | tee -a $SCRIPT_LOG_FILE
	echo -e "\e[1;31m"CRITICAL ERROR:: Errors in SFTTrace.log file"\e[0m" | tee -a $EMAIL_OUT_FILE
	tail -10 /logs/sft/Script_logs/DB_error_check.`hostname`.`date '+%m%d%Y'`.log | grep "Critical error" | tee -a $EMAIL_OUT_FILE
    else
	echo "There are no errors in SFTTrace.log file" >> $SCRIPT_LOG_FILE
    fi

    tail -10 /logs/sft/Script_logs/CDTestfile_error_check.`hostname`.`date '+%m%d%Y'`.log | grep -q "Critical error"
    if [ $? -eq 0 ]
    then
        echo -e "\n" | tee -a $SCRIPT_LOG_FILE
        echo -e "\e[1;31m"CRITICAL ERROR::Errors in CDtest.log file"\e[0m" | tee -a $EMAIL_OUT_FILE
        tail -10 /logs/sft/Script_logs/CDTestfile_error_check.`hostname`.`date '+%m%d%Y'`.log | grep "Critical error"| tee -a $EMAIL_OUT_FILE
    else
        echo "There are no errors in CDtest.log file" >> $SCRIPT_LOG_FILE
    fi

    # Validating any stranded files in ControlFiles directory
    find /amex/data/push/controlfiles -type f \( -mmin +15 \)  -ls > $SCRIPT_LOG_DIR/controlfiles
    if [ -s $SCRIPT_LOG_DIR/controlfiles ]
    then
	echo -e "\n" | tee -a $EMAIL_OUT_FILE
        echo -e "\e[1;34m"------------------------------------Stranded files in /amex/data/push/controlfiles-----------------------------"\e[0m" | tee -a $EMAIL_OUT_FILE
	echo -e "\e[1;31m"CRITICAL ERROR:: There are `wc -l $SCRIPT_LOG_DIR/controlfiles | awk '{print $1}'` files in /amex/data/push/controlfiles form more 15 mins.Please check for any issues with outbound file transmission."\e[0m" | tee -a $EMAIL_OUT_FILE
    else
	echo "There are no stranded files in /amex/data/push/controlfiles" >> $SCRIPT_LOG_FILE
    fi

    # Validating file descriptor values for TM and SFT Container
    fdfilename=/tmp/`hostname -s`_fhcount.log
    if [ -e $fdfilename ]
    then       
       echo "------------------------------------FILE DESCRIPTOR VALUES FOR TM AND SFT CONTAINER-----------------------------" >>  $SCRIPT_LOG_FILE
       echo -e "\n" >> $SCRIPT_LOG_FILE
       echo "TRANSACTION MANAGER Thread count :: `tail -1 $fdfilename | awk '{print $2,"  @ " $1}'`" >> $SCRIPT_LOG_FILE
       echo "CONTAINER Thread count :: `tail -1 $fdfilename | awk '{print $4,"  @ " $1}'`" >>  $SCRIPT_LOG_FILE
    fi

    #Validates for defunct state process
    if ps -eLf | grep -v grep | grep defunct > /dev/null
    then
     echo -e "\n" | tee -a $EMAIL_OUT_FILE
     echo -e "\e[1;34m"------------------------------------DEFUNCT STATE PROCESS DETAILS-----------------------------------------------"\e[0m" | tee -a $EMAIL_OUT_FILE
       echo -e "\e[1;31m"CRITICAL below mentioned process went into defunct state. Please check ASAP."\e[0m" | tee -a $EMAIL_OUT_FILE
       echo "CRITICAL below mentioned process went into defunct state. Please check ASAP." >> $SCRIPT_LOG_FILE 
       ps -eLf | grep -v grep | grep defunct | tee -a $EMAIL_OUT_FILE
    fi

    echo -e "\n" | tee -a $EMAIL_OUT_FILE
    #cd /amex/SFT/Container/scripts
    
    # Validating SFT Queue Depth
    echo -e "\e[1;34m"------------------------------------Queue depth on `hostname -s`-----------------------------------------------------"\e[0m" | tee -a  $EMAIL_OUT_FILE
    if [ `whoami` != root ] ; then
       echo "Please enter your login password if required" | tee -a $EMAIL_OUT_FILE
       sudo /amex/SFT/Container/scripts/SFT_QueueDepth.sh | tee -a $SCRIPT_LOG_FILE
    else
       /amex/SFT/Container/scripts/SFT_QueueDepth.sh | tee -a $SCRIPT_LOG_FILE
    fi
    echo -e "\n" >> $SCRIPT_LOG_FILE
    
    # Validating CPU Usage
    echo "------------------------------------Top CPU processes and system resource usage  on `hostname -s`---------------" >> $SCRIPT_LOG_FILE
    #top | head -10 >> $SCRIPT_LOG_FILE
    
  # x=`date | awk '{print $4}' | cut -d ':' -f1`
    #if [ $x -eq 12 ];
     #   then
      #     y=`date -d "1 days ago" "+%d"`	    
       #    sar -f /var/log/sa/sa$y | tail -1 | awk '{print $4}' > $SCRIPT_LOG_DIR/tmp1_`hostname -s`
        #   CPU_USAGE_PERCENT=`cat $SCRIPT_LOG_DIR/tmp1_`hostname -s`
         #  sar -f /var/log/sa/sa$y | tail -1 | awk '{print $9}' > $SCRIPT_LOG_DIR/tmp2_`hostname -s`
          # CPU_IDLE_PERCENT=`cat $SCRIPT_LOG_DIR/tmp2_`hostname -s`
          # sar -f /var/log/sa/sa$y | tail -1 | awk '{print $7}' > $SCRIPT_LOG_DIR/tmp3_`hostname -s`
          # CPU_IO_WAIT_PERCENT=`cat $SCRIPT_LOG_DIR/tmp3_`hostname -s`
          # cat /var/log/sa/sar$y | grep -A 143 "runq-sz" | tail -1 | awk '{print $4, $5, $6}' > $SCRIPT_LOG_DIR/tmp4_`hostname -s`
          # CPU_LOAD_AVERAGE=`cat $SCRIPT_LOG_DIR/tmp4_`hostname -s`
          # rm $SCRIPT_LOG_DIR/tmp1_`hostname -s` $SCRIPT_LOG_DIR/tmp2_`hostname -s` $SCRIPT_LOG_DIR/tmp3_`hostname -s` $SCRIPT_LOG_DIR/tmp4_`hostname -s`
     
   # else

           CPU_USAGE_PERCENT=$(echo "`sar | tail -1 | awk '{print $3}'`") 
           CPU_IDLE_PERCENT=$(echo "`sar | tail -1 | awk '{print $8}'`") 
           CPU_IO_WAIT_PERCENT=$(echo "`sar | tail -1 | awk '{print $6}'`") 
           CPU_LOAD_AVERAGE=$(echo "`sar -q | less | tail -1 | awk '{print $4, $5, $6}'`") 
  # fi
    
    echo -e "\n" | tee -a $SCRIPT_LOG_FILE
    echo -e "\e[1;34m"--------------------------------------CPU Usage Details-----------------------------------------------------------"\e[0m" | tee -a $SCRIPT_LOG_FILE
  
   echo "CURRENT CPU USAGE :  $CPU_USAGE_PERCENT%" | tee -a $SCRIPT_LOG_FILE
    
    echo "CURRENT CPU IDLE TIME :  $CPU_IDLE_PERCENT%" | tee -a $SCRIPT_LOG_FILE
   
    echo "CURRENT CPU IO WAIT TIME :  $CPU_IO_WAIT_PERCENT%" | tee -a $SCRIPT_LOG_FILE
   #echo "CURRENT NO OF ZOMBIE PROCESS RUNNING : " $ZOMBIE_PROCESS | tee -a $SCRIPT_LOG_FILE
  echo "CURRENT LOAD AVERAGE IS :  $CPU_LOAD_AVERAGE" | tee -a $SCRIPT_LOG_FILE
  

 echo -e "\e[1;34m"------------------------------------------------------------------------------------------------------------------"\e[0m" | tee -a $SCRIPT_LOG_FILE

#    if [[ ${CPU_USAGE_PERCENT} -gt 40 ]]
#    then
#	echo "HIGH CPU USAGE"	
#    fi
    
    echo "Apart from the above validation ,please perform the below checks manually to confirm Application status " >> $SCRIPT_LOG_FILE
    echo "       1). Send test files thru VIP internal and external" >> $SCRIPT_LOG_FILE
    echo "       2). Send test files using virtual user screen on individual servers both FE and BE" >> $SCRIPT_LOG_FILE
    echo "       3). Send test file using sftp connection " >> $SCRIPT_LOG_FILE
    echo "       4). Check stranded files if needed " >> $SCRIPT_LOG_FILE
    echo " 	 5). Check for any currupted trusted CAs from the Admin GUI" >> $SCRIPT_LOG_FILE
    echo "	 6). Verify the latest outbound file transfer from Primary server" >> $SCRIPT_LOG_FILE
    echo "       7). Verify the folder_monitor_error_file_list.log in LPPMA652 server" >> $SCRIPT_LOG_FILE
    echo "----------------------------------------------------------------------------------------------------------------" >> $SCRIPT_LOG_FILE

    #########
    #Purge old logs in ${SCRIPT_LOG_DIR} that are 5 days older
    #########
    echo -e "\n" >> $SCRIPT_LOG_FILE
    echo "---------------------------------Deleting old log files from $SCRIPT_LOG_FILE directory--------------" >> $SCRIPT_LOG_FILE
    if [ -d $SCRIPT_LOG_DIR ]
    then
       echo "Searching for old log files under $SCRIPT_LOG_DIR" >> $SCRIPT_LOG_FILE
       for i in `find $SCRIPT_LOG_DIR/lppma* -type f -mtime +$LOG_MAX_TTL 2>/dev/null`
       do
           echo "Cleaning up Old Logs morethan 5 days" $i >> $SCRIPT_LOG_FILE
           rm -f $i
       done
    else
       echo "$SCRIPT_LOG_DIR Directory not found in $SYSTEM_NAME." >> $SCRIPT_LOG_FILE
    fi
    echo "----------------------------------------------------------------------------------------------------------------" >> $SCRIPT_LOG_FILE

#    echo -e "\n" >>  $SCRIPT_LOG_FILE
 #   echo "Thanks," >> $SCRIPT_LOG_FILE
  #  echo "SFT Production Support." >> $SCRIPT_LOG_FILE
#    cp $EMAIL_OUT_FILE /amex/data/users/00000044280/outbox/SFT_ALERT_EMAIL.$$.$RANDOM

#To Check the MQ Connection Counts

echo -e "\n" >> $SCRIPT_LOG_FILE
echo "---------------------------------Checking the MQ Connection Counts on `hostname -s`---------------" >> $SCRIPT_LOG_FILE
MQ_count=`tail -1 /logs/mqm/connection-count.log | awk '{print $7}'`
if [ $MQ_count -gt 1500 ];
then
echo -e "\n" >>  $SCRIPT_LOG_FILE
echo "MQ connection is $MQ_count on `hostname -s`" >> $SCRIPT_LOG_FILE
echo "*******************************************************************"
echo -e "\n" 
echo "MQ connection is $MQ_count on `hostname -s`, Please verify this ASAP" >> $EMAIL_OUT_FILE
echo "MQ count is more than 1500, please verify this ASAP" >>$SCRIPT_LOG_FILE 
else
echo "MQ connection is $MQ_count on `hostname -s`" >> $SCRIPT_LOG_FILE
fi

hostname1=`hostname -s`
if [ $hostname1 == lppma652d ] 
then
       echo -e "\n"
       echo "******************************************Checking Folder Monitor*********************************************"  | tee -a $SCRIPT_LOG_FILE
       Year=`tail -1 $Folder_Monitor  | awk '{print $1}' | cut -d '/' -f3`
       Month=`tail -1 $Folder_Monitor | awk '{print $1}' | cut -d '/' -f1`
       Date=`tail -1 $Folder_Monitor | awk '{print $1}' | cut -d '/' -f2`
       Hour=`tail -1 $Folder_Monitor | awk '{print $2}' | cut -d ':' -f1`
       Minutes=`tail -1 $Folder_Monitor | awk '{print $2}' | cut -d ':' -f2`
       Second=`tail -1 $Folder_Monitor | awk '{print $2}' | cut -d ':' -f3`
       folder_timestamp=$Year$Month$Date$Hour$Minutes$Second
       #echo "$folder_timestamp"
       secondsDiff=$(( `date '+%Y%m%d%H%M%S'` - $folder_timestamp ))  
       #echo "$secondsDiff"
       if [ $secondsDiff -gt 2000 ] 
           then
       echo "There is no folder monitor alert for last 30 minutes" >>$SCRIPT_LOG_FILE
           else
       echo "There is a folder Monitor alert, please check ASAP" >>$EMAIL_OUT_FILE
       echo "There is a folder Monitor alert, please check ASAP" >>$SCRIPT_LOG_FILE
      fi
else
echo " It is running on the server other than LPPMA652D" >>$SCRIPT_LOG_FILE
fi

echo -e "\n"
echo "**********************************************Checking Cron Alerts File*********************************************" | tee -a $SCRIPT_LOG_FILE

Year=`tail -1 $cron_alerts  | awk '{print $1}' | cut -d '/' -f3`
       Month=`tail -1 $cron_alerts | awk '{print $1}' | cut -d '/' -f1`
       Date=`tail -1 $cron_alerts | awk '{print $1}' | cut -d '/' -f2`
       Hour=`tail -1 $cron_alerts | awk '{print $2}' | cut -d ':' -f1`
       Minutes=`tail -1 $cron_alerts | awk '{print $2}' | cut -d ':' -f2`
       Second=`tail -1 $cron_alerts | awk '{print $2}' | cut -d ':' -f3`
       folder_timestamp=$Year$Month$Date$Hour$Minutes$Second
       #echo "$folder_timestamp"
       secondsDiff=$(( `date '+%Y%m%d%H%M%S'` - $folder_timestamp ))
       #echo "$secondsDiff"
       Last_Line_Read=`cat $SCRIPT_LOG_DIR/counter_$HOST_NAME`
       Current_Lines_Number=`less $cron_alerts | wc -l`
       n=`expr $Current_Lines_Number - $Last_Line_Read`   
       if [ $secondsDiff -gt 2000 ]
           then 
       echo "There is no alert in cron alert file for last 20 minutes"
           else
          tail -$n $cron_alerts | grep -v "PermissionsCheckAgent" 
		  greprc=$?
             if [ $greprc -eq 0 ]; then
              #echo "There is no alert in cron alert file for last 20 minutes"
              echo "There is an alert in Cron alert, please check ASAP" >>$EMAIL_OUT_FILE
              echo "There is an alert in Cron alert, please check ASAP" | tee -a $SCRIPT_LOG_FILE
              else
	          #echo "There is an alert in Cron alert, please check ASAP" >>$EMAIL_OUT_FILE	 
                  #echo "There is an alert in Cron alert, please check ASAP" | tee -a $SCRIPT_LOG_FILE
                  echo "There is no alert in cron alert file for last 20 minutes"
              fi
             fi
         less $cron_alerts | wc -l > $SCRIPT_LOG_DIR/counter_$HOST_NAME 

as2=`cat $xferlogpath/xferlog | grep "as2" | grep -v ".subscriptions" | wc -l`
ssh=`cat $xferlogpath/xferlog | grep "ssh" | grep -v ".subscriptions" | wc -l`
ftp=`cat $xferlogpath/xferlog | grep "ftp" | grep -v ".subscriptions" | wc -l`
https=`cat $xferlogpath/xferlog | grep "http" | grep -v ".subscriptions" | wc -l`
STOutbound=`cat $xferlogpath/xferlog | grep ".subscriptions" | wc -l`
CD=`less $CDlogspath | grep "C program execution successful." | wc -l`
echo "AS2=$as2" >>$SCRIPT_LOG_FILE
echo "SSH=$ssh" >>$SCRIPT_LOG_FILE
echo "FTP=$ftp" >>$SCRIPT_LOG_FILE
echo "HTTPS=$https" >>$SCRIPT_LOG_FILE
echo "STOutbound=$STOutbound" >>$SCRIPT_LOG_FILE
echo "CD=$CD" >>$SCRIPT_LOG_FILE

rm $SCRIPT_LOG_DIR/xferlog_process_`hostname -s`
rm $SCRIPT_LOG_DIR/CD_Process_`hostname -s`

for (( i = 10; i >=0; i-- )) ; do
e=`date -d "$i minutes ago" "+%H:%M"`
less $xferlogpath/xferlog | grep "$e" >>$SCRIPT_LOG_DIR/xferlog_process_`hostname -s`
less $CDlogspath | grep "$e" >>$SCRIPT_LOG_DIR/CD_Process_`hostname -s`
done

as2_10min=`cat $SCRIPT_LOG_DIR/xferlog_process_`hostname -s` | grep "as2" | grep -v ".subscriptions" | wc -l`
ssh_10min=`cat $SCRIPT_LOG_DIR/xferlog_process_`hostname -s` | grep "ssh" | grep -v ".subscriptions" | wc -l`
ftp_10min=`cat $SCRIPT_LOG_DIR/xferlog_process_`hostname -s` | grep "ftp" | grep -v ".subscriptions" | wc -l`
https_10min=`cat $SCRIPT_LOG_DIR/xferlog_process_`hostname -s` | grep "http" | grep -v ".subscriptions" | wc -l`
STOutbound_10min=`cat $SCRIPT_LOG_DIR/xferlog_process_`hostname -s` | grep ".subscriptions" | wc -l`
CD=`less $SCRIPT_LOG_DIR/CD_Process_`hostname -s` | grep "C program execution successful." | wc -l`

echo "AS2=$as2_10min" >>$SCRIPT_LOG_FILE
echo "SSH=$ssh_10min" >>$SCRIPT_LOG_FILE
echo "FTP=$ftp_10min" >>$SCRIPT_LOG_FILE
echo "HTTPS=$https_10min" >>$SCRIPT_LOG_FILE
echo "STOutbound=$STOutbound_10min" >>$SCRIPT_LOG_FILE
echo "CD=$CD_10min" >>$SCRIPT_LOG_FILE

cp $EMAIL_OUT_FILE /amex/data/users/00000044280/outbox/SFT_ALERT_EMAIL.$$.$RANDOM
echo -e "\n" >>  $SCRIPT_LOG_FILE
    echo "Thanks," >> $SCRIPT_LOG_FILE
    echo "SFT Production Support." >> $SCRIPT_LOG_FILE

cp $SCRIPT_LOG_FILE $SCRIPT_LOG_DIR/`hostname -s`

a="lppma653d"
hostname=`hostname -s`
if [ $hostname != "$a" ]
then
exit 1
else
sleep 180
cd $SCRIPT_LOG_DIR
sh Data_Combine_Validation_Script.sh
sleep 120
cd $SCRIPT_LOG_DIR
sh HTML_Validation.sh
sleep 5
cd $SCRIPT_LOG_DIR
sh CPU_Script.sh
fi

#b="lppma652d"
#hostname=`hostname -s`
#if [ $hostname != "$b" ]
#then
#exit 1
#else
#sleep 180
#cd /amex/g1data/Support_Scripts/Dashboard
#sh Dashboard_SFT.sh 
#fi

