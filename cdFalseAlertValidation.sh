#!/bin/ksh
BASE_DIR=/amex/data/SupportScripts/cdTransCheck
logs_dir=$BASE_DIR/logs
tmp_dir=$BASE_DIR/tmp
scrptnm="cdFalseAlertValidation.sh"
#hostname information
hostnm_fil=$BASE_DIR/hostname.properties
#app user
usernm=`whoami`
tmphsnm_fil="temphostname.properties"
hostname="`hostname`"

input_file="tid.properties"
tm_infil="tid.properties`date '+%H%M%S'`"
falsalrtfil="$logs_dir/false_alert"
missedalrt_fil="$logs_dir/missed_tids"
missedalrt_tmp="$logs_dir/missed_tids_tmp"
cd_logs_path="/amex/cdunix/work/sftcdprd/S*"
msg="MSST=Copy step successful"
srptlogs="$logs_dir/cdFalseAlert.log`date '+%H%M%S'`"
LOG_MAX_TTL=7

#
checkcdlogs()
{
   if [ ${#1} -eq 0 ]
   then
      # Rename old files
      if [ -e $falsalrtfil ]
      then
          mv $falsalrtfil $falsalrtfil`date '+%H%M%S'`
      fi
      if [ -e $missedalrt_fil ]
      then
          mv $missedalrt_fil $missedalrt_fil`date '+%H%M%S'`
      fi 
      while read line
      do
         grep "$line" $cd_logs_path | tr '|' '\n' | grep "$msg" | grep -v Local > /dev/null
         if [ $? -eq 0 ]
         then
            echo $line --'>' False alert
            echo $line >> $falsalrtfil
         else
            echo $line >> $missedalrt_fil
         fi
      done < $BASE_DIR/$input_file
   else
      while read line
      do
         grep "$line" $cd_logs_path | tr '|' '\n' | grep "$msg" | grep -v Local > /dev/null
         if [ $? -eq 0 ]
         then            
            echo $line --'>' False alert
            echo $line >> $falsalrtfil
         else
            echo $line "check in other server" >> $srptlogs
         fi
      done < $missedalrt_fil
   fi
   sleep 2
}

#
removeflsalrt()
{
   #cat $tmp_dir/$tmphsnm_fil | grep -v $hostname >> $tmp_dir/$tmphsnm_fil
   #to remove false alert tid from next missed alert file if any
     if [ -e $missedalrt_fil ]
     then
         if [ -e $falsalrtfil ]
         then
            for line in $(cat $missedalrt_fil); do
               grep "$line" $falsalrtfil > /dev/null
               if [ $? -eq 0 ]
               then
                  echo $line >> $srptlogs              
               else 
                  echo $line >> $missedalrt_tmp
               fi
            done
            
            if [ -e $missedalrt_tmp ]
            then
               rm $missedalrt_fil
               cat $missedalrt_tmp | uniq -u >> $missedalrt_fil
               rm $missedalrt_tmp
            else
               rm $missedalrt_fil
            fi
         fi
     fi
}

if [ ${#1} -eq 0 ]
then
   echo "======================================================= "
   echo "          C:D False Alert validation utility"
   echo "======================================================= "
   cat $hostnm_fil | grep -v $hostname >> $tmp_dir/$tmphsnm_fil
   echo -e "\e[1;34m"Script is now checking in $hostname"\e[0m" | tee -a $srptlogs

   # Take backup file
   cp $BASE_DIR/$input_file $tmp_dir/$tm_infil
   # Check false alert in script running server
   checkcdlogs
    
   if  [ -e $missedalrt_fil ]
   then
      for servernm in $(cat $tmp_dir/$tmphsnm_fil); do
        if  [ -e $missedalrt_fil ]
        then
           echo " "
           echo -e "\e[1;34m"Script is now checking in $servernm"\e[0m" | tee -a $srptlogs
           ssh -n -oport=446 $usernm@$servernm 2>./amxbanner.txt "$BASE_DIR/$scrptnm '$missedalrt_fil'"
        fi
      done
      echo " "
      echo "======================================================= "
      if  [ -e $falsalrtfil ]
      then
           no_fls_alrt=`wc -l $falsalrtfil | cut -d ' ' -f1`
           echo -e " Number of false alert      : \e[1;32m"$no_fls_alrt"\e[0m"
      fi
      if  [ -e $missedalrt_fil ]
      then
           no_msd_alrt=`wc -l $missedalrt_fil | cut -d ' ' -f1`
           echo -e " Number of missed alert     : \e[1;31m"$no_msd_alrt"\e[0m"
      else
           echo    " Number of missed alert     : 0"
      fi
      if  [ -e $input_file ]
      then
           no_of_alrt=`wc -l $input_file | cut -d ' ' -f1`
           echo -e " Total no of files verified : \e[1;33m"$no_of_alrt"\e[0m"
      fi
      echo "======================================================= "
   else
      echo "Script execution completed"
   fi
else
   checkcdlogs $1
   removeflsalrt
fi

#
if [ -e $missedalrt_fil ]
then
   chmod 777 $missedalrt_fil
fi


if [ -e $falsalrtfil ]
then
   chmod 777 $falsalrtfil
fi


# Removing tmp files
if [ -e $tmp_dir/$tmphsnm_fil ]
then
  rm $tmp_dir/$tmphsnm_fil
fi

    #########
    #Purge old logs in ${logs_dir} that are 7 days older
    #########
    #cho -e "\n" >> $logs_dir
    #cho "---------------------------------Deleting old log files from $SCRIPT_LOG_FILE directory--------------" >> $SCRIPT_LOG_FILE
    if [ -d $logs_dir ]
    then
       #cho "Searching for old log files under $SCRIPT_LOG_DIR" >> $SCRIPT_LOG_FILE
       for i in `find ${logs_dir} -type f -mtime +$LOG_MAX_TTL 2>/dev/null`
       do
           #cho "Cleaning up Old Logs morethan 7 days" $i 
           rm -f $i
       done
    #lse
     # echo "$logs_dir Directory not found in $SYSTEM_NAME." 
    fi

