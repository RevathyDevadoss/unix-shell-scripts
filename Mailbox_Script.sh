#!/bin/bash

FILELOGS_PATH="/amex/data/SupportScripts/Mailbox_Script/"
USERS_LIST="/amex/data/users/00000239547/outbox/SFT_User_Names_List"

if [ -f $FILELOGS_PATH/physicalFileNames.txt ];
then
rm $FILELOGS_PATH/physicalFileNames.txt;
fi


LOGALERT_TIME=`date '+%H:%M:%S'`
LOG_DATE=`date '+%m%d%Y'`
HOST_NAME="`hostname`"
#LOG_FILE_NAME="$FILELOGS_PATH/Mailbox_Logs/mailboxLog.$user_id.$HOST_NAME.$LOG_DATE.$LOGALERT_TIME..log"


get_userID(){

        echo -e "\\n\\n    Enter the SFT mailbox user numeric ID :  "
        read user_id
#	 user_id=""

                if echo $user_id | egrep -q '^[0-9]+$';
                then user_id=$(printf "%011d" $user_id)
                        USER_DIR="/amex/data/users/$user_id/"
                        OUTBOX="/amex/data/users/$user_id/outbox"
                        SENT="/amex/data/users/$user_id/sent"
                        LOG_FILE_NAME="$FILELOGS_PATH/Mailbox_Logs/mailboxLog.$user_id.$HOST_NAME.$LOG_DATE.$LOGALERT_TIME.log"
                        operations
                else echo -e "\\n\\n    User ID entered is invalid. Please enter valid numeric user ID. "
                        get_userID
                fi

}

operations(){

        if [ -d $USER_DIR ]

        then clear

        echo -e "\\n\\n    Please select an operation to perform on mailbox files       \\n"

                while :

                do

                echo -e "\\n\\n    1) Resend files to outbox\\n    2) Move files from outbox to sent\\n    3) Delete files permanently\\n    4) Exit\\n\\n    Enter your option: "
                echo -e "    \\t "

                read from_to

                case $from_to in

                1)

                        while read file_name
                        do `cd $SENT`

                        cd $SENT

                                if [ -f $OUTBOX/$file_name ];
                                then echo "$file_name is already available in outbox" >> $LOG_FILE_NAME;

                                else if [ -f $file_name  ];
                                        then repo_filePath=`ls -ltr $file_name|awk '{print $11}'`

                                        ln -fs $repo_filePath $OUTBOX/$file_name

                                        echo -e "\\n\\n $file_name is now placed in outbox" >> $LOG_FILE_NAME;

                                        else
                                        echo -e "\\n\\n $file_name is not available in SFT as per SFT retention period" >> $LOG_FILE_NAME;

                                        fi
                                fi

                        done < $FILELOGS_PATH/file_mailbox.properties;

                echo -e "\\n\\n    Selected option to resend files to outbox. Operation has been completed\\n\\n"

                exit;;

                2)

                while read file_name
                do `cd $OUTBOX`

                cd $OUTBOX

                        if [ -f $file_name  ];
                        then repo_filePath=`ls -ltr $file_name|awk '{print $11}'`

                        ln -fs $repo_filePath $SENT/$file_name
                        rm $file_name;
                        echo "$file_name is now moved to sent" >> $LOG_FILE_NAME

                        else if [ -f $SENT/$file_name  ];
                                then echo "$file_name is in sent but not in outbox" >> $LOG_FILE_NAME;
                                else echo "$file_name is not available in SFT as per SFT retention period" >> $LOG_FILE_NAME;
                                fi
                        fi

                done < $FILELOGS_PATH/file_mailbox.properties;

                echo -e "\\n\\n    Selected option to move files from outbox to sent. Operation has been completed.\\n\\n"

                exit;;

                3)

                clearFromMailbox(){

                echo -e "\\n\\n\\n    You have choosen to delete files permanently from user mailbox       \\n\\n"

                while :

                do

                echo -e "\\n\\n     3.1) Delete files from outbox\\n     3.2) Delete files from sent\\n     3.3) Exit\\n\\n     Enter your option : "
                echo -e "    \\t "

                read remove

                case $remove in

                        3.1)

                        echo -e "\\n\\n    You have choosed to delete files from outbox."

                                while read file_name1
                                do `cd $OUTBOX/`
                                        if [ -f $OUTBOX/$file_name1 ];
                                        then rm_repo_filePath=`ls -ltr $OUTBOX/$file_name1|awk '{print $11}'`
                                                rm $OUTBOX/$file_name1;

                                                echo "$file_name1 is now deleted from outbox" >> $LOG_FILE_NAME;
                                                echo "$file_name1" >> $FILELOGS_PATH/nowRemoved_files.txt;
                                                echo "$rm_repo_filePath" >> $FILELOGS_PATH/fileRepoPaths.txt;

                                        else echo "$file_name1 is not available in SFT as per SFT retention period" >> $LOG_FILE_NAME;
                                        fi

                                        if [ -f $SENT/$file_name1  ]
                                        then rm $SENT/$file_name1
                                        fi

                                done < $FILELOGS_PATH/file_mailbox.properties;

                                if [ -f $FILELOGS_PATH/nowRemoved_files.txt ];
                                then echo -e "\\n\\n    Selected option to delete files from outbox. Operation has been completed.\\n\\n"
                                        removePermanent
                                else echo -e "\\n\\n    No files found to be removed.\\n\\n"
                                        exit
                                fi
                        exit;;

                        3.2)

                        echo -e "\\n\\n    You have choosed to delete files from sent."

                                while read file_name1
                                do `cd $SENT/`

                                        if [ -f $SENT/$file_name1 ];
                                        then rm_repo_filePath=`ls -ltr $SENT/$file_name1|awk '{print $11}'`
                                                rm $SENT/$file_name1;

                                                echo "$file_name1 is now deleted from sent" >> $LOG_FILE_NAME;
                                                echo "$file_name1" >> $FILELOGS_PATH/nowRemoved_files.txt;
                                                echo "$rm_repo_filePath" >> $FILELOGS_PATH/fileRepoPaths.txt

                                        else echo "$file_name1 is not available in SFT as per SFT retention period" >> $LOG_FILE_NAME;
                                        fi

                                        if [ -f $OUTBOX/$file_name1  ]
                                        then rm $OUTBOX/$file_name1
                                        fi

                                done < $FILELOGS_PATH/file_mailbox.properties;

                                if [ -f $FILELOGS_PATH/nowRemoved_files.txt ];
                                then echo -e "\\n\\n    Selected option to delete files from sent. Operation has been completed.\\n\\n"
                                        removePermanent
                                else echo -e "\\n\\n    No files found to be removed.\\n\\n"
                                        exit
                                fi
                        exit;;

                        3.3) clear

                        echo -e "\\n\\n    You have choosen to exit from sub menu."

                        operations;;

                        *) clear

                        echo -e "\\n\\n Sorry. You have entered invalid option. Ensure entering correct option."

                        ;;

                        esac

                        done
                }

                clear

                clearFromMailbox

                ;;

                4) echo -e "\\n   You have choosen to exit from main menu. Bye\\n\\n";
                   exit;;

                *) clear
                echo -e "\\n\\n  Sorry. You have entered invalid option. Ensure entering correct option.\\n\\n"
                 ;;

                esac

                done

        else echo -e "\\n\\n  Sorry. You have entered invalid user ID. The entered user ID is not available in SFT.\\n\\n"
                get_userID

        fi

}

removePermanent(){

                if [ -f $FILELOGS_PATH/nowRemoved_files.txt ]
                        then echo -e "\\n\\n     Files are no more available in user mailbox. Are you sure want to delete the listed files permanently from SFT?\\n\\n"
                        cat $FILELOGS_PATH/nowRemoved_files.txt;
                        confirm
                else echo -e "\\n\\n    No files found to be removed.\\n\\n"
                fi

        if [ -f $FILELOGS_PATH/nowRemoved_files.txt ]
        then rm $FILELOGS_PATH/nowRemoved_files.txt;
        fi

}

confirm(){

        echo -e "\\n\\n     Enter your option : (Y/N)?\\n\\n"

        read option

                if [ "$option" == "Y" ]
                then echo -e "\\n\\n    All the related links were deleted. Kindly check script logs\\n\\n"

                                while read repositoryPath
                                        do physicalFilename=`basename $repositoryPath`
                                        echo "$physicalFilename" >> $FILELOGS_PATH/physicalFileNames.txt;
                                        rm $repositoryPath;
                                        #rm /amex/data/outbound/hardlinks/$physicalFilename*;
                                        #rm $repositoryPath;
                                done < $FILELOGS_PATH/fileRepoPaths.txt

                        echo -e "\\n\\n    All the links to corresponding files were deleted permanently. Please delete hardlinks through NFS server. Check physicalFileNames.txt in the script logs for repository file names\\n\\n"

                        if [ -f $FILELOGS_PATH/fileRepoPaths.txt ]
                        then rm $FILELOGS_PATH/fileRepoPaths.txt
                        fi

                else echo -e "\\n\\n    Not recommended. Please enter Y to delete files permanently."
                        confirm

                fi

}

get_userID


