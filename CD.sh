#!/bin/sh
export LD_LIBRARY_PATH=/opt/mqm/lib

#!/bin/sh
export LD_LIBRARY_PATH=/opt/mqm/lib

# load the configuration file, changes path from /amex/data to /amex/ngp/data
. /amex/ngp/data/cd/scripts/connectdirect.properties

#-- NFS re-mount chnages start--
. /amex/ngp/data/cd/scripts/connectdirectFileSys.properties

## get the Username & Full filepath
FULL_FILE_PATH=$1
USERNAME=$2
STATUS="end"
TEMPEXT=".tmp"
PROCESSID=`ps -p $$ -o ppid=`
export GLOBAL_TID;
GPFS_FilePath="/amex/data/users/"
# Create Log Dir
mkdir -p $CD_LOGDIR



## validation method
validate(){
  if [ -f ${FULL_FILE_PATH} ]; then # checking only for file existence
    # file exist, put logic here
    RETURN_CODE=0
        writeToLog "Validation Success:: Source File exists."
  else
    raiseAlert "Source file doesn't exist: ${FULL_FILE_PATH}, while executing $0 script."
    RETURN_CODE=1
        writeToLog "Validation Failed:: Source File doesn't exist."
    return ${RETURN_CODE};
  fi
  # 2. Validate whether ${FOLDER_MONITOR_ROOT_DIR}${USERNAME}/ folder exist or not -- Raise Alert
  if [ -d "${FOLDER_MONITOR_ROOT_DIR}/${USERNAME}" ]; then # check for the existence of directory
    # directroy exist, we are good
        writeToLog "Validation Success:: Folder Monitor Directory exists. ${FOLDER_MONITOR_ROOT_DIR}/${USERNAME}"
    RETURN_CODE=0
  else
    # directory doesn't exist, need to raise an alert
    raiseAlert "Directory doesn't exist: ${FOLDER_MONITOR_ROOT_DIR}/${USERNAME}, while executing $0 script."
        writeToLog "Validation Failed:: Folder Monitor Directory doesn't exists. ${FOLDER_MONITOR_ROOT_DIR}/${USERNAME}"
                strandedFile $FULL_FILE_PATH $USERNAME
    RETURN_CODE=2
  fi

 # FILEPATH_USERID=$(echo "${FULL_FILE_PATH}" | egrep "[0-9]{11}" --only-matching)
  FILEPATH_USERID=`echo ${FULL_FILE_PATH} | cut -d"/" -f5`
  FMD_USERID=$(readlink "${FOLDER_MONITOR_ROOT_DIR}/${USERNAME}" | egrep "[0-9]{11}" --only-matching)
  if [ "${FILEPATH_USERID}" != "${FMD_USERID}" ]; then
    writeToLog "USERNAME AND USERID MISMATCH::: USER: ${USERNAME}  FILENAME: $(basename ${FULL_FILE_PATH}) FILEPATH_USERID: ${FILEPATH_USERID}  FMD_USERID: ${FMD_USERID}"
  fi

  return ${RETURN_CODE};
}

generateTid(){
  ## it will generate TID using uffi.jar

(
flock -x 200

  java -cp /amex/st/brules/local/agents/inprocess/scripts/ufii.jar:/amex/st/brules/local/agents/inprocess/scripts/CDTIDGenerator.jar:/amex/st/brules/local/agents/inprocess/scripts: com.americanexpress.connectdirect.core.TIDGenerator
)200>/logs/sft/.cdincoming_end.synchronizer

  RETURN_CODE=$?
  TID=$(cat ${TID_FILE_NAME})
#echo "`date +'%m%d%Y%T'` $FULL_FILE_PATH $TID">>/var/tmp/dupplicate_tid_test.logs
  return ${RETURN_CODE};

}


moveToTemp(){

FILE_PATH_DIR=$(dirname "${FULL_FILE_PATH}")
#f [ $"{FILE_PATH_DIR}" != ${GPFS_FilePath}???????????/???????????????? ] || [ "${FILE_PATH_DIR}" != ${TEMP_FOLDER_PATH}???????/???????????/???????????????? ];then
#f [ "${FILE_PATH_DIR}" != ${File1} ] || [ "${FILE_PATH_DIR}" != ${File2} ];then
if [[ ! $FILE_PATH_DIR == $GPFS_FilePath???????????/???????????????? ]] && [[ ! $FILE_PATH_DIR == $TEMP_FOLDER_PATH???????/???????????/???????????????? ]];then
    FILE_NAME=$(basename "${FULL_FILE_PATH}")
    ## Create a temporary folder under file base directory path
    TEMP_FOLDER=$FILE_PATH_DIR/`date +%Y%j%H%M%S%N | cut -c4-19`
    writeToLog "`date +'%m/%d/%Y %T'` Temp folder is : $TEMP_FOLDER"
    mkdir $TEMP_FOLDER
        chmod 777 $TEMP_FOLDER
        if [ $? -ne 0 ]
        then
                writeToLog  "Create folder ${TEMP_FOLDER} failed. Stopping the script."
                raiseAlert  "Not able to create the temp folder: $TEMP_FOLDER, while executing $0 script."
                strandedFile $FULL_FILE_PATH $USERNAME
                exit 1
        fi

    writeToLog " File move from $FULL_FILE_PATH to $TEMP_FOLDER started"
    ## Move file to temp directory to avoid customer impact while uploading same file name
    if [ -f ${FULL_FILE_PATH} ]; then
    mv $FULL_FILE_PATH $TEMP_FOLDER/$FILE_NAME
                if [ $? -ne 0 ]
                then
                        writeToLog  "Failed to move file $FULL_FILE_PATH to  ${TEMP_FOLDER}. Stopping the script."
                        raiseAlert  "Failed to move file $FULL_FILE_PATH to $TEMP_FOLDER ."
                        strandedFile $FULL_FILE_PATH $USERNAME
                        rm -rf $TEMP_FOLDER
                        if [ $? -ne 0 ]
                        then
                                writeToLog  "Failed to remove temporary directory ${TEMP_FOLDER}. Stopping the script."
                        else
                                writeToLog  "Removed ${TEMP_FOLDER} directory, as it is not being used. Stopping the script."
                                exit 1
                        fi
                fi
    else
                writeToLog "${FULL_FILE_PATH} is not a file."
    fi
    ## Set Full file to temporary path now, which will be refered in fruther execution
    writeToLog " File move from $FULL_FILE_PATH to $TEMP_FOLDER completed"
    FULL_FILE_PATH=$TEMP_FOLDER/$FILE_NAME
    MOVE_TO_TEMP_STATUS=$?
else
writeToLog " File is alreday present in temp direcory $FULL_FILE_PATH"
MOVE_TO_TEMP_STATUS=$?
fi
return ${MOVE_TO_TEMP_STATUS};



}


moveToPickup(){
 FILE_NAME=$(basename "${FULL_FILE_PATH}")
 NEW_FULL_FILE_NAME="${FILE_NAME}#${TID}"
 writeToLog "Moving File  :: ${FULL_FILE_PATH} to ${FOLDER_MONITOR_ROOT_DIR}/${USERNAME}/${NEW_FULL_FILE_NAME}"
 if [ -f ${FULL_FILE_PATH} ]; then
 mv "${FULL_FILE_PATH}" "${FOLDER_MONITOR_ROOT_DIR}/${USERNAME}/${NEW_FULL_FILE_NAME}"
 MOVE_STATUS=$?
 else
 MOVE_STATUS=1
 writeToLog "${FULL_FILE_PATH} is not a file."
 fi
writeToLog "Moving status is $MOVE_STATUS"
 if [ ${MOVE_STATUS} -eq "0" ]; then
writeToLog "Starting the stat command as file is moved"
    writeToLog "File Moved successfully:: ${FOLDER_MONITOR_ROOT_DIR}/${USERNAME}/${NEW_FULL_FILE_NAME} With File Size:: $(stat -c %s ${FOLDER_MONITOR_ROOT_DIR}/${USERNAME}/${NEW_FULL_FILE_NAME})"
writeToLog "stat command is finished"
TMP_DIR_NAME=$(dirname "${FULL_FILE_PATH}")
    writeToLog "Removing temporary directory $TMP_DIR_NAME"
    rmdir $TMP_DIR_NAME
        MOVE_STATUS=$?
  else
    raiseAlert "File Move Failed:: From ${FULL_FILE_PATH} to ${FOLDER_MONITOR_ROOT_DIR}/${USERNAME}/${NEW_FULL_FILE_NAME}"
         strandedFile $FULL_FILE_PATH $USERNAME
fi


  return ${MOVE_STATUS};
}
cdWebServiceCall(){
    writeToLog "Getting the PICK PATH with User ID for file name:: ${FOLDER_MONITOR_ROOT_DIR}/${USERNAME}/${NEW_FULL_FILE_NAME}"
    FULL_FMD_USERID_PATH=$(readlink "${FOLDER_MONITOR_ROOT_DIR}/${USERNAME}")
    WS_PICK_FILE_PATH=${FULL_FMD_USERID_PATH}/${NEW_FULL_FILE_NAME}
    writeToLog "Successfully Constructed PICKUP File Path: ${WS_PICK_FILE_PATH} for cdmonitor file name: ${FOLDER_MONITOR_ROOT_DIR}/${USERNAME}/${NEW_FULL_FILE_NAME} "
    ENCODED_FULL_FILE_NM=$(echo "$WS_PICK_FILE_PATH" | sed -e 's/ /%20/g' -e 's/\//%2f/g' -e 's/#/%23/g' -e 's/_/%5F/g' -e 's/-/%2D/g')


    #writeToLog "Going to post WebService Request with File Name :${WS_PICK_FILE_PATH} with WebService call $WS_CALL_URL "
    WS_LOCAL_COUNT=1
    while [ $WS_LOCAL_COUNT -le $CD_WS_MAX_RETRY_COUNT ]
    do
    CD_WS_URL=${CD_WS_PROTOCOL}${CD_WS_HOST_NM}:${CD_WS_PORT}/${CD_WS_ENDPOINT}
    WS_CALL_URL=${CD_WS_URL}${ENCODED_FULL_FILE_NM}${INSTANCE_ID_PARAM}${CD_WS_HOST_NM}

    writeToLog "Going to post WebService Request with File Name :${WS_PICK_FILE_PATH} with WebService call $WS_CALL_URL Attempt :${WS_LOCAL_COUNT}/${CD_WS_MAX_RETRY_COUNT} "

    CD_WS_RESPONSE=$(curl -X GET "$WS_CALL_URL")
    if echo "$CD_WS_RESPONSE" | grep -q "$CD_WS_SUCCESS_RESPONSE"
        then
        writeToLog "Successfully Posted WebService Request with File Name :${WS_PICK_FILE_PATH} with WebService call $WS_CALL_URL "
        FILE_TRANSFER=TRUE
        break
        return 0;
    else
        writeToLog "Failed to post WebService Request on instance ID:${CD_WS_HOST_NM} and File Name :${WS_PICK_FILE_PATH} with WebService call $WS_CALL_URL , retrying with another CD servers in cluster Error Message:\n $CD_WS_RESPONSE "
        SERVER_NAME=$(echo "`hostname -s`_CD_WS_Sites")
        SITE_NAME="${!SERVER_NAME}"
        for SERVER_HOST in $(echo ${!SITE_NAME} | tr "," "\n")
        do
            if [ "$SERVER_HOST" != "`hostname -s`" ];then
                writeToLog "Retrying WebService Request on instance ID:${SERVER_HOST} for File Name :${WS_PICK_FILE_PATH} "
                CD_WS_URL=${CD_WS_PROTOCOL}${SERVER_HOST}:${CD_WS_PORT}/${CD_WS_ENDPOINT}
                WS_CALL_URL=${CD_WS_URL}${ENCODED_FULL_FILE_NM}${INSTANCE_ID_PARAM}${SERVER_HOST}
                CD_WS_RESPONSE=$(curl -X GET "$WS_CALL_URL")
                if echo "$CD_WS_RESPONSE" | grep -q "$CD_WS_SUCCESS_RESPONSE"
                then
                    writeToLog "Successfully Posted WebService Request with File Name :${WS_PICK_FILE_PATH} with WebService call $WS_CALL_URL "
                    FILE_TRANSFER=TRUE
                    break 2
                    return 0;
                else
                    writeToLog "Failed to Post WebServices Call on Instance ID:${SERVER_HOST} for file name:${WS_PICK_FILE_PATH} and API URL:${WS_CALL_URL} Error Message:\n $CD_WS_RESPONSE"
                fi
            fi
        done
    fi

    WS_LOCAL_COUNT=`expr $WS_LOCAL_COUNT + 1`
    if [ $WS_LOCAL_COUNT -le $CD_WS_MAX_RETRY_COUNT ]
    then
    writeToLog "Failed to Post WebService Request on all CD instances:${!SITE_NAME} for File Name :${WS_PICK_FILE_PATH}, will auto retry after wait time of $WS_CD_WAIT_TIMER seconds"
    sleep $WS_CD_WAIT_TIMER
    fi
    done

    if [ "$FILE_TRANSFER" != "TRUE" ];then
    writeToLog "Failed to Post WebService Request on all CD instances:${!SITE_NAME} for File Name :${WS_PICK_FILE_PATH} with WebService call $WS_CALL_URL "
    raiseAlert "Failed to Post WebService Request on all CD instances:${!SITE_NAME} for File name :: ${WS_PICK_FILE_PATH}"
    strandedFile ${FOLDER_MONITOR_ROOT_DIR}/${USERNAME}/${NEW_FULL_FILE_NAME} $USERNAME
    return 1;
    fi

}

strandedFile()
{
echo "$1 $2 FAILED" >> $CD_FILE_RECOVERY
writeToLog "File transfer is on hold for file $FULL_FILE_PATH and username $USERNAME"
}

raiseAlert(){
  echo "$(date +'%m/%d/%Y %T') $ALERT_CD $INFOMAN_GRP ${SEV_LEVEL} ${1}" >>${ALERTFILE}
}

writeToLog(){
  echo "`date +'%m/%d/%Y %T'` [CDIncomingEnd:$FULL_FILE_PATH $USERNAME] [INFO] [$1] " >> $LOGFILE
}

writeToLog " Starting file processing $FULL_FILE_PATH for user $USERNAME $STATUS $PROCESSID "

##Commenting checkMigrationStatus to disable user migration check
#checkMigrationStatus
#STATUS=$?
#writeToLog " STATUS is $STATUS"
#if [ "${STATUS}" -eq "1" ];
#then
#exit;
#fi

## Now start the processing
moveToTemp
STATUS=$?
if [ "${STATUS}" -eq "0" ]; then
  # validation success, proceed with TID generation
        validate
        STATUS=$?
        if [ "${STATUS}" -eq "0" ]; then

        #(
        #flock -x 200
        generateTid
      # )200>/logs/sft/.cdincoming_end.synchronizer
                STATUS=$?
                if [ "${STATUS}" -eq "0" ]; then
                writeToLog "TID generated Successfully:: $TID"
                moveToPickup
                STATUS=$?
                else
                                # need to raise the alert
                                writeToLog "TID generation failed."
                                raiseAlert "TID generation failed:: $FULL_FILE_PATH, while executing $0 script."
                                strandedFile $FULL_FILE_PATH $USERNAME
                fi
                if [ "${STATUS}" -eq "0" ]; then
                cdWebServiceCall

                fi
#       )200>/logs/sft/.cdincoming_end.synchronizer
        fi
fi
echo $STATUS;
