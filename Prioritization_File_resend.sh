#Script to resend the file
#!/bin/bash
DATE=`date '+%m%d%Y'`
TIME=`date '+%H:%M:%S'`
SCRIPT_WORK_DIRECTORY="/amex/data/SupportScripts/StrandedFilesScript"
users_directory="/amex/data/users"
Stranded_files=$1
LOG_PATH="/amex/data/SupportScripts/StrandedFilesScript/logs"
SCRIPT_LOG_FILE="$LOG_PATH/$Stranded_files.$DATE.log"
cd_script="sudo -u tumbleweed /amex/st/brules/local/agents/inprocess/scripts/CDIncomingEnd.sh"
if [ -e $SCRIPT_WORK_DIRECTORY/`basename $0`.$Stranded_files.FLAG ]
then
        echo "Error: An instance of this script is already running!"
        exit 1
else
        echo -n "How many seconds needed between every file resend : "
                read SLEEP_TIME
          X=`echo $SLEEP_TIME | tr -dc  '[:digit:]' `
        touch $SCRIPT_WORK_DIRECTORY/`basename $0`.$Stranded_files.FLAG
        echo "$DATE $TIME File_resend.sh script has started" >> $SCRIPT_LOG_FILE
        #To check whether Strandedfile_list and SFT_User_Names_List availabble
        if [ -e $SCRIPT_WORK_DIRECTORY/$Stranded_files -a -e $SCRIPT_WORK_DIRECTORY/SFT_User_Names_List ];
        then
                echo "$DATE $TIME Strandedfiles list and SFT usernames list are available under $SCRIPT_WORK_DIRECTORY.Please continue with file transfer" >> $SCRIPT_LOG_FILE
        else
                echo "$DATE $TIME Required files are not available.Please check" >> $SCRIPT_LOG_FILE
                rm $SCRIPT_WORK_DIRECTORY/`basename $0`_FLAG
                exit 2;
        fi

        for full_file_name in `cat $SCRIPT_WORK_DIRECTORY/$Stranded_files`
        do
                FILE_NAME=`basename $full_file_name`
                echo "----------------------------------------------------------------------" >> $SCRIPT_LOG_FILE
                echo "$DATE $TIME Started  resending the file $FILE_NAME" >> $SCRIPT_LOG_FILE
                USER_ID=`echo $full_file_name | cut -d '/' -f6`
                echo "$DATE $TIME USER_ID=$USER_ID" >> $SCRIPT_LOG_FILE
                USER_NAME="`grep $USER_ID $SCRIPT_WORK_DIRECTORY/SFT_User_Names_List | awk '{print $1}'`"
                echo "$DATE $TIME USER_NAME=$USER_NAME" >> $SCRIPT_LOG_FILE
                #To check the location of stranded file.If it is in temporary/inbox directory move the file to users home directory
                if [ -e $users_directory/$USER_ID/$FILE_NAME ];
                then
                        echo "$DATE $TIME File is in users home directory" >> $SCRIPT_LOG_FILE
                else
                        echo "$DATE $TIME File is in inbox/temporary directory" >> $SCRIPT_LOG_FILE
                        mv $full_file_name $users_directory/$USER_ID
                        if [ $? == 0 ]
                        then
                                echo "$DATE $TIME File Moved successfully to the user home directory" >> $SCRIPT_LOG_FILE
                        else
                                echo "$DATE $TIME Failed to move the $full_file_name.Please check" >> $SCRIPT_LOG_FILE
                                echo "$full_file_name failed to move to users directory.Please verify" >> $SCRIPT_WORK_DIRECTORY/FAILED_FILES_LIST.$$
                                continue
                        fi
                fi
                #Run the CDIncomingEnd script. If user name is not found dont send the file from user dir.
                if [ ${#USER_NAME} -gt 0 ]
                then
                cd_string="$cd_script $users_directory/$USER_ID/$FILE_NAME $USER_NAME"
                echo "Connect Direct String : $cd_string"
                status=`$cd_string`
                echo $status
                #To check the status of the file transfer
                if [ $status -eq 2 ]
                then
                     echo "$DATE $TIME $full_file_name Transfer is successful." >> $SCRIPT_LOG_FILE
                else
                     echo "$DATE $TIME $users_directory/$USER_ID/$FILE_NAME not sent from users directory. Please verify." >> $SCRIPT_LOG_FILE
                     echo "$full_file_name" >> $SCRIPT_WORK_DIRECTORY/FAILED_FILES_LIST.$$
                fi
                sleep $X
                else
                      echo "$DATE $TIME $users_directory/$USER_ID/$FILE_NAME not sent from users directory. Please verify." >> $SCRIPT_LOG_FILE
                      echo "$full_file_name" >> $SCRIPT_WORK_DIRECTORY/FAILED_FILES_LIST.$$
                fi
        done
        mv  $SCRIPT_WORK_DIRECTORY/$Stranded_files $LOG_PATH/Resent_$Stranded_files
        if [ -s $SCRIPT_WORK_DIRECTORY/FAILED_FILES_LIST.$$ ]
        then
            echo "**********************************************************************" | tee -a $SCRIPT_LOG_FILE
            echo "${BOLD} PlEASE VERIFY BELOW FILES AS THESE FILES WERE FAILED TO TRANSFER  ${NORM} " | tee -a $SCRIPT_LOG_FILE
            echo "------------------------------------------------------------------------" | tee -a $SCRIPT_LOG_FILE
                echo " `cat $SCRIPT_WORK_DIRECTORY/FAILED_FILES_LIST.$$`" | tee -a $SCRIPT_LOG_FILE
            echo "------------------------------------------------------------------------" | tee -a $SCRIPT_LOG_FILE
        fi
        fi
rm $SCRIPT_WORK_DIRECTORY/`basename $0`.$Stranded_files.FLAG
