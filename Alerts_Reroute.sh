#!/bin/sh
CurrDate=`date '+%m%d%Y'`
CurrTime=`date '+%H:%M:%S'`
SCRIPT_WORK_DIRECTORY=/amex/data/SupportScripts/SupportScripts_alerts
#SCRIPT_WORK_DIRECTORY=/tmp/ETTScript
HOST_NAME="`hostname`"
LOG_DIRECTORY=/logs/sft
#LOG_DIRECTORY=/tmp/ETTScript
Log="$LOG_DIRECTORY/Script_logs/Alerts_RerouteFile.$HOST_NAME.$CurrDate.log"
#Log="$LOG_DIRECTORY/Alerts_RerouteFile.$HOST_NAME.$CurrDate.log"
Email="/amex/data/users/00000044280/outbox/SFT_ALERT_EMAIL.$$.$RANDOM"
DB_OP=/amex/data/users/00000214306/outbox/Database_output
#DB_OP=$SCRIPT_WORK_DIRECTORY/Database_output
ERRORLIST=$SCRIPT_WORK_DIRECTORY/Error_classification
WG=$SCRIPT_WORK_DIRECTORY/Workgroups
Alertlog=$LOG_DIRECTORY/alerts.log
SFTTracelog=$LOG_DIRECTORY/SFTTrace.log
Last_hour_SFTTracelog=$LOG_DIRECTORY/SFTTrace_Rotate/SFTTrace.log.`date -d "-1 hour 15 min ago" +%F-%H`
#Last_hour_SFTTracelog=$LOG_DIRECTORY/SFTTrace.log.`date -d "-1 hour 15 min ago" +%F-%H`
Alertslog=$SCRIPT_WORK_DIRECTORY/Alerts.log_$HOST_NAME-`date +%b_%d_%H:%M`
Final_Alertslog=$LOG_DIRECTORY/Final_Alerts.log
ERRORLOGS=$SCRIPT_WORK_DIRECTORY/File_Error.$HOST_NAME.log
NEWLOGS=$SCRIPT_WORK_DIRECTORY/NEW_Alerts.$HOST_NAME.log
LAST_ALERT=$SCRIPT_WORK_DIRECTORY/Last_searched_alert_$HOST_NAME
AlertCodesFile=$SCRIPT_WORK_DIRECTORY/alertcodefile
AlertsCodeList=`cat $AlertCodesFile`
DEFAULT_SEVRITY=SEV-3
File_exist()
{
if [ ! -e $1 ]; then

    MSG_STRING1="File $1 is not available for script execution in `hostname -a` server."
    MSG_STRING2="Please check the files. Exiting from the script !!"
    MSG_STRING3="**********************************************************************************************"
    echo -e "$MSG_STRING1" >> $Log
    echo -e "$MSG_STRING2" >> $Log
    echo "$MSG_STRING3" >> $Log
    echo "$MSG_STRING1" "$MSG_STRING2" >> $Email
    rm $SCRIPT_WORK_DIRECTORY/`basename $0`_$HOST_NAME-FLAG
    exit 0;
fi
}

WORKGROUP()
{
        #echo "Inside Workgroup function..."
     Partner=$2
     grep -w "$Partner" $DB_OP |  tr -d '[:blank:]' | awk -F ',' '{ for (i=0;i<=NF;i++) if( $i == "'$Partner'" )print $(i+1)}'| grep -qowf $WG
     if [ $? -eq 0 ]
     then
        Alert="`grep $1 $Alertslog | grep $3 | sed -n '1p'`"
        ERROR_CODE="`echo $Alert | awk '{print $3}'`"
        SevLevel="`echo $Alert | awk '{print $5}'`"
        PrivClass="`echo $Alert | awk '{print $4}'`"
        #echo $SevLevel $ERROR_CODE
        WorkGroup="`grep -w "$Partner" $DB_OP |  tr -d '[:blank:]' | awk -F ',' '{ for (i=0;i<=NF;i++) if( $i == "'$Partner'" )print $(i+2)}'`"
        #echo "WG"$WorkGroup
        echo $Alert | sed 's/'$ERROR_CODE'/SFT_9999/g' | sed 's/'$PrivClass'/'$WorkGroup'/g' | sed 's/'$SevLevel'/'$DEFAULT_SEVRITY'/g' >> $NEWLOGS
        echo -e "- File WorkGroup belongs to $WorkGroup Privilege class" >>$Log
    else
        grep $1 $Alertslog | grep $3 | sed -n '1p' >> $NEWLOGS
        echo -e "- File $1 does not belongs to mentioned workgroups" >> $Log
    fi
}

ERROR()
{
        grep $1 $2 > $ERRORLOGS
        case `grep -of $ERRORLIST $ERRORLOGS | sed -n '1p'` in
        'Message ID  => SCPA024I') SUserName="`grep "Message ID  => SCPA024I" $ERRORLOGS | sed -n '1p' | awk '{print $3}' | tr -d '[]'| tail -1`"
                                   grep -wq "$SUserName" $DB_OP
                                   if  [ $? -eq 0 ]
                                   then
                                        echo -e "- File :$3 Source User name:$SUserName ERROR:Record length failure" >> $Log
                                        WORKGROUP $3 $SUserName $4
                                   else
                                        echo -e "- File :$3 Source User name:$SUserName ERROR:Record length failure" >> $Log
                                        echo "Source User name $SUserName is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
        # MSE2 Change
        'Message ID  => SCPA072I') SUserName="`grep "Message ID  => SCPA072I" $ERRORLOGS | sed -n '1p' | awk '{print $3}' | tr -d '[]'| tail -1`"
                                   grep -wq "$SUserName" $DB_OP
                                   if  [ $? -eq 0 ]
                                   then
                                        echo -e "- File :$3 Source User name:$SUserName ERROR:Record length failure" >> $Log
                                        WORKGROUP $3 $SUserName $4
                                   else
                                        echo -e "- File :$3 Source User name:$SUserName ERROR:Record length failure" >> $Log
                                        echo "Source User name $SUserName is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;

        'Message ID  => SVSH021I') SUserName="`grep "Message ID  => SVSH021I" $ERRORLOGS | sed -n '2p' | awk '{print $3}' | tr -d '[]'| tail -1`"
                                   grep -wq "$SUserName" $DB_OP
                                   if  [ $? -eq 0 ]
                                   then
                                        echo -e "- File :$3 Source User name:$SUserName ERROR:Block Size failure" >> $Log
                                        WORKGROUP $3 $SUserName $4
                                   else
                                        echo -e "- File :$3 Source User name:$SUserName ERROR:Block Size failure" >> $Log
                                        echo "Source User name $SUserName is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;

        'Message ID  => SVSH018I') SUserName="`grep "Message ID  => SVSH018I" $ERRORLOGS | sed -n '2p' | awk '{print $3}' | tr -d '[]'| tail -1`"
                                   grep -wq "$SUserName" $DB_OP
                                   if  [ $? -eq 0 ]
                                   then
                                        echo -e "- File :$3 Source User name:$SUserName ERROR:Block Size failure" >> $Log
                                        WORKGROUP $3 $SUserName $4
                                   else
                                        echo -e "- File :$3 Source User name:$SUserName ERROR:Block Size failure" >> $Log
                                        echo "Source User name $SUserName is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;

        'SAFB013I') SUserName="`grep "SAFB013I" $ERRORLOGS | sed -n '2p' | awk '{print $3}' | tr -d '[]'| tail -1`"

                                        echo "Checking for SAFB013I  in $ERRORLOGS" >> $Log
                                   grep -wq "$SUserName" $DB_OP
                                   if  [ $? -eq 0 ]
                                   then
                                        echo -e "- File :$3 Source User name:$SUserName ERROR:Invalid Script Paramater" >> $Log
                                        WORKGROUP $3 $SUserName $4
                                   else
                                        echo -e "- File :$3 Source User name:$SUserName ERROR:Invalid Script Paramater" >> $Log
                                        echo "Source User name $SUserName is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;


         'XPAM001I') SUserName="`grep "XPAM001I" $ERRORLOGS | sed -n '2p' | awk '{print $3}' | tr -d '[]'| tail -1`"

                                        echo "Checking for XPAM001I in $ERRORLOGS" >> $Log
                                   grep -wq "$SUserName" $DB_OP
                                   if  [ $? -eq 0 ]
                                   then
                                        echo -e "- File :$3 Source User name:$SUserName ERROR:Invalid Script Paramater" >> $Log
                                        WORKGROUP $3 $SUserName $4
                                   else
                                        echo -e "- File :$3 Source User name:$SUserName ERROR:Invalid Script Paramater" >> $Log
                                        echo "Source User name $SUserName is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;

        'Message ID  => SDE035CI') SUserName="`grep "Message ID  => SDE035CI" $ERRORLOGS | sed -n '1p' | awk '{print $3}' | tr -d '[]'| tail -1`"
                                   grep -wq "$SUserName" $DB_OP
                                   if  [ $? -eq 0 ]
                                   then
                                        echo -e "- File :$3 Source User name:$SUserName ERROR:Invalid dataset name" >> $Log
                                        WORKGROUP $3 $SUserName $4
                                   else
                                        echo -e "- File :$3 Source User name:$SUserName ERROR:Invalid dataset name" >> $Log
                                        echo "Source User name $SUserName is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
        'Message ID  => SVSL003I') x="`grep "DISP=(,CATLG)" $ERRORLOGS | sed -n '2p' | awk '{print $3}' | tr -d '[]'| tail -1`"
                                   grep -oqw '^[0]*'$x'' $DB_OP
                                   if  [ $? -eq 0 ]
                                   then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File :$3 Destination Transmitter ID:$DxmitId ERROR:Dataset already cataloged" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File :$3 ERROR:Dataset already cataloged" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;

        # MSE2 Changes
        'Message ID  => SCIA011I') x="`grep "Message ID  => SCIA011I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:SignOn Failed" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:SignOn Failed" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;

         'Message ID  => SVTB004I') x="`grep "Message ID  => SVTB004I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:SignOn Failed" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:SignOn Failed" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;


        'Message ID  => SITA340I') x="`grep "Message ID  => SITA340I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:Storage Allocation Failed" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:Storage Allocation Failed" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
        'Message ID  => SITA341I') x="`grep "Message ID  => SITA341I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:Storage Allocation Failed" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:Storage Allocation Failed" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;


        'Message ID  => SITA342I') x="`grep "Message ID  => SITA342I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:Storage Allocation Failed" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:Storage Allocation Failed" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
        'Message ID  => RACF095I') x="`grep "Message ID  => RACF095I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:Not authorized to access the dataset" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:Not authorized to access the dataset" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;

        'Message ID  => SDAA001I') x="`grep "Message ID  => SDAA001I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:Syntax Error Allocating User File" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:Syntax Error Allocating User File" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;


        'Message ID  => SDAA004I') x="`grep "Message ID  => SDAA004I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:Syntax Error Allocating User File" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:Syntax Error Allocating User File" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
        'Message ID  => SDAA005I') x="`grep "Message ID  => SDAA005I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:Syntax Error Allocating User File" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:Syntax Error Allocating User File" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;

        'Message ID  => SDAA048I') x="`grep "Message ID  => SDAA048I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:Syntax Error Allocating User File" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:Syntax Error Allocating User File" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;


        'Message ID  => SCBB001I') x="`grep "Message ID  => SCBB001I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SCBC030I') x="`grep "Message ID  => SCBC030I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SCBD001I') x="`grep "Message ID  => SCBD001I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SCBE001I') x="`grep "Message ID  => SCBE001I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SCBF001I') x="`grep "Message ID  => SCBF001I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SCBF063I') x="`grep "Message ID  => SCBF063I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SCBF064I') x="`grep "Message ID  => SCBF064I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SCBG001I') x="`grep "Message ID  => SCBG001I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SCBH001I') x="`grep "Message ID  => SCBH001I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SCBI001I') x="`grep "Message ID  => SCBI001I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SCBJ001I') x="`grep "Message ID  => SCBJ001I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SCBK005I') x="`grep "Message ID  => SCBK005I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SCBL001I') x="`grep "Message ID  => SCBL001I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SCBN001I') x="`grep "Message ID  => SCBN001I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SCBO001I') x="`grep "Message ID  => SCBO001I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SCBP001I') x="`grep "Message ID  => SCBP001I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SCBR002I') x="`grep "Message ID  => SCBR002I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SCBS001I') x="`grep "Message ID  => SCBS001I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SCBT005I') x="`grep "Message ID  => SCBT005I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SCBU003I') x="`grep "Message ID  => SCBU003I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SCBV001I') x="`grep "Message ID  => SCBV001I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SCBW001I') x="`grep "Message ID  => SCBW001I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SCBX001I') x="`grep "Message ID  => SCBX001I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SCBY001I') x="`grep "Message ID  => SCBY001I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SCPA008I') x="`grep "Message ID  => SCPA008I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SFIA002I') x="`grep "Message ID  => SFIA002I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SFIA003I') x="`grep "Message ID  => SFIA003I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SRJA014I') x="`grep "Message ID  => SRJA014I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SRTA008I') x="`grep "Message ID  => SRTA008I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
                'Message ID  => SSUB100I') x="`grep "Message ID  => SSUB100I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:not authorized to perform COPY function" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:not authorized to perform COPY function" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                  ;;

        'isHOLD') x="`grep "isHOLD" $ERRORLOGS | sed -n '1p' | awk '{print $4}' | tr -d '[]'| tail -1`"
                  grep -owq '^[0]*'$x'' $DB_OP
                  if  [ $? -eq 0 ]
                  then
                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                        echo -e "- File :$3 Destination Transmitter ID:$DxmitId ERROR:Node down issue" >> $Log
                        echo $DxmitId >> $Log
                        WORKGROUP $3 $DxmitId $4
                  else
                        echo -e "- File :$3 ERROR:Node down issue" >> $Log
                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                  fi
                  ;;
        'SDE9700I') SUserName="`grep SDE9700I $ERRORLOGS | sed -n '1p' | awk '{print $3}' | tr -d '[]' | tail -1 `"
                    grep -wq "$SUserName" $DB_OP
                    if  [ $? -eq 0 ]
                    then
                        echo -e "- File :$3 Source Username:$SUserName ERROR:Racf ID Issue" >> $Log
                        WORKGROUP $3 $SUserName $4
                    else
                        echo -e "- File :$3 Source Username:$SUserName ERROR:Racf ID Issue" >> $Log
                        echo "Source User name $SUserName is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                    fi
                    ;;
        'Push Failed : File Push To Remote Server Failed') DUserName="`grep  "Push Failed : File Push To Remote Server Failed" $ERRORLOGS | sed -n '1p' | awk '{print $3}' | tr -d '[]' | tail -1 `"
                                                   grep -wq "$DUserName" $DB_OP
                                                   if  [ $? -eq 0 ]
                                                   then
                                                         echo -e "- File :$3 Destination User name:$DUserName ERROR: Push Failure" >> $Log
                                                         WORKGROUP $3 $DUserName $4
                                                    else
                                                         echo -e "- File :$3 Destination User name:$DUserName ERROR: Push Failure" >> $Log
                                                         echo "Destination User name $DUserName is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                                         grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                                    fi
                                                    ;;
        'Message ID  => SDAA009I') x="`grep "Message ID  => SDAA009I" $ERRORLOGS | sed -n '1p'| awk '{print $4}' | tr -d '[]' | tail -1`"
                                  grep -owq '^[0]*'$x'' $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        DxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File:$3 Destination Transmitter ID:$DxmitId ERROR:Invalid/Non Existant GDG Issue" >> $Log
                                        echo $DxmitId >> $Log
                                        WORKGROUP $3 $DxmitId $4
                                   else
                                        echo -e "- File:$3  ERROR:Invalid/Non Existant GDG Issue" >> $Log
                                        echo "Destination Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
        'MAILBOX STEP: Error placing file in') x="`grep "MAILBOX STEP: Error placing file" $ERRORLOGS | awk '{print $6}' | tr -d '[]' | tail -1`"
                                               grep -owq '^[0]*'$x'' $DB_OP
                                               if  [ $? -eq 0 ]
                                               then
                                                        SxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                                        echo -e "- File:$3 Source Transmitter ID:$SxmitId ERROR:Mailbox Failure" >> $Log
                                                        echo $SxmitId >> $Log
                                                        WORKGROUP $3 $SxmitId $4
                                                else
                                                        echo -e "- File:$3  ERROR:Mailbox Failure" >> $Log
                                                        echo "Source Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                                fi
                                                ;;
        'CATALOG STEP: failed')x="`grep "CATALOG STEP: failed" $ERRORLOGS | awk '{print $6}' | tr -d '[]' | tail -1`"
                               grep -oqw '^[0]*'$x'' $DB_OP
                               if  [ $? -eq 0 ]
                               then
                                        SxmitId=`grep -ow '^[0]*'$x'' $DB_OP | sed -n '1p'`
                                        echo -e "- File :$3 Source Transmitter ID:$SxmitId ERROR:Catalog stetp Failure" >> $Log
                                        echo $SxmitId >> $Log
                                        WORKGROUP $3 $SxmitId $4
                                else
                                        echo -e "- File :$3  ERROR:Catalog stetp Failure" >> $Log
                                        echo "Source Transmitter ID $x is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                fi
                                    ;;
        'Scheduled Retrieval failed') SUserName="`grep  "Scheduled Retrieval failed" $ERRORLOGS | awk '{print $5}' | tr -d '[]' | tail -1`"
                                  grep -wq "$SUserName" $DB_OP
                                  if  [ $? -eq 0 ]
                                  then
                                        echo -e "- File :$3 Source User Name:$SUserName Error:Scheduled Retrieval Failure" >> $Log
                                        WORKGROUP $3 $SUserName $4
                                  else
                                        echo -e "- File :$3 Source User Name:$SUserName Error:Scheduled Retrieval Failure" >> $Log
                                        echo "Source User name $SUserName is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
                                        grep $1 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
                                   fi
                                   ;;
        #'FileFlowId is NULL')SUserName="`grep "FileFlowId is NULL" $ERRORLOGS | awk '{print $6}' | tr -d '[]' | sed -n '1p'`"
        #                    grep -wq "$SUserName" $DB_OP
        #                    if  [ $? -eq 0 ]
        #                    then
        #                           echo -e "- File :$3 Source User Name:$SUserName Error:Invalid file uploaded by the AS2 User " >> $Log
        #                           WORKGROUP $3 $SUserName $4
        #                     else
        #                       echo -e "- File :$3 Source User Name:$SUserName Error:Invalid file uploaded by the AS2 User " >> $Log
        #                       echo "Source User name $SUserName is not present in $DB_OP file.so writing to Final alerts UNCHANGED" >> $Log
        #                   fi
        #                   ;;
        *)echo -e "- No matching error found..copying the alert to Final_alerts.log UNCHANGED !! " >> $Log
          grep $3 $Alertslog | grep $4 | sed -n '1p' >> $NEWLOGS
          ;;
esac
}
if [ -e $SCRIPT_WORK_DIRECTORY/`basename $0`_$HOST_NAME-FLAG ]
then
        echo "Error: An instance of this script is already running!"
        exit 1
else
        echo "*********************************************************************************************" >> $Log
        echo "Script started" at `date '+%m/%d/%Y'` `date '+%H:%M:%S'` >> $Log
        touch $SCRIPT_WORK_DIRECTORY/`basename $0`_$HOST_NAME-FLAG
fi
File_exist $DB_OP
File_exist $ERRORLIST
File_exist $WG
File_exist $SFTTracelog
File_exist $Last_hour_SFTTracelog
File_exist $Alertlog
File_exist $AlertCodesFile
# Reading the alert codes to be scaned in alerts.log file
#while read alertcodes
#do
#echo "Reading the aleret code file"
#AlertsCodeList=alertcodes
#echo -e "Alerts codes to be scanned in alerts.log file $AlertsCodeList" >> $Log
#done<$AlertCodesFile

LOG_SEARCH_FILE="$SCRIPT_WORK_DIRECTORY/Alert_search_start_indicator.`hostname`"
if [ ! -s $LOG_SEARCH_FILE ]
then
    echo "0" > $LOG_SEARCH_FILE
fi
SEARCH_START="`cat $LOG_SEARCH_FILE`"
FILE_SIZE="`cat $Alertlog | wc -l`"
NEW_SEARCH=0
if [ $FILE_SIZE -ge $SEARCH_START ]
then
        NEW_SEARCH=`expr $FILE_SIZE - $SEARCH_START`
else
        NEW_SEARCH=`expr $FILE_SIZE - 0`
fi

if [ $NEW_SEARCH -gt 0 ]
then
        echo "$CurrDate $CurrTime - There are few NEW ALERTS after the last run" >> $Log
        echo  "------------------------------------------------------------------------------------------------------ " >> $Log
        if [ -s $LAST_ALERT ]
        then
                grep -q "`cat $LAST_ALERT`" $Alertlog
                if  [ $? -eq 0 ]
                then
                        tail -$NEW_SEARCH $Alertlog  > $Alertslog
                else
                        LMDATE=$(stat --format=%y $Final_Alertslog | awk '{print $1}' | cut -d '-' -f 1,3);
                        TDATE=`date +%Y-%d`
                        if [ $LMDATE == $TDATE ]
                        then
                                cat $Final_Alertslog > $Final_Alertslog`date -d "1 day ago" +%Y-%d`
                        else
                                cat $Final_Alertslog > $Final_Alertslog$LMDATE
                        fi
                        #cat $Final_Alertslog > $Final_Alertslog`date -d "1 day ago" +%Y-%d`
                        > $Final_Alertslog
                        Priviousday_alerts=$LOG_DIRECTORY/alerts.log`date -d "1 day ago" "+%Y-%d"`
                        Alert=`cat $LAST_ALERT`
                        LineNumber=`grep -n "$Alert" $Priviousday_alerts | cut -d ':' -f1`
                        sed -n ''$LineNumber',$p' $Priviousday_alerts | sed 1d > $Alertslog
                        tail -$FILE_SIZE $Alertlog  >> $Alertslog
                fi
        else
                tail -$NEW_SEARCH $Alertlog  > $Alertslog
        fi
elif [ $NEW_SEARCH -eq 0 ]
then
        if [ -s $LAST_ALERT ]
        then
                grep -q "`cat $LAST_ALERT`" $Alertlog
                if  [ $? -eq 0 ]
                then
                        echo "$CurrDate $CurrTime - There are NO NEW ALERTS after the last run" >> $Log
                        echo "Script Ended" at `date '+%m/%d/%Y'` `date '+%H:%M:%S'` >> $Log
                        rm $SCRIPT_WORK_DIRECTORY/`basename $0`_$HOST_NAME-FLAG
                        exit 0
                else
                        LMDATE=$(stat --format=%y $Final_Alertslog | awk '{print $1}' | cut -d '-' -f 1,3);
                        TDATE=`date +%Y-%d`
                        if [ $LMDATE == $TDATE ]
                        then
                                cat $Final_Alertslog > $Final_Alertslog`date -d "1 day ago" +%Y-%d`
                        else
                                cat $Final_Alertslog > $Final_Alertslog$LMDATE
                        fi
                                #cat $Final_Alertslog > $Final_Alertslog$LMDATE
                                #cat $Final_Alertslog > $Final_Alertslog`date -d "1 day ago" +%Y-%d`
                        > $Final_Alertslog
                #       echo "check for sed error 1"
                        Priviousday_alerts=$LOG_DIRECTORY/alerts.log`date -d "1 day ago" "+%Y-%d"`
                        Alert=`cat $LAST_ALERT`
                        LineNumber=`grep -n "$Alert" $Priviousday_alerts | cut -d ':' -f1`
                        sed -n ''$LineNumber',$p' $Priviousday_alerts | sed 1d > $Alertslog
                #       echo " Check for sed error 2"
                        tail -$FILE_SIZE $Alertlog  >> $Alertslog
                fi
         fi
else
         echo "$CurrDate $CurrTime - There are NO NEW ALERTS after the last run" >> $Log
         echo "Script Ended" at `date '+%m/%d/%Y'` `date '+%H:%M:%S'` >> $Log
         echo "*********************************************************************************************" >> $Log
         rm $SCRIPT_WORK_DIRECTORY/`basename $0`_$HOST_NAME-FLAG
         exit 0
fi
while read alert
do
        echo "$alert" | egrep -qow $AlertsCodeList>>$Log
        if  [ $? -eq 0 ]
        then
                echo -e "\n" >> $Log
                echo ">> Reading the new alert -- "$alert"">> $Log
                Month1=$(echo "$alert" | awk '{print $1}' | cut -d '/' -f1 );
                Date1=$(echo "$alert" | awk '{print $1}' | cut -d '/' -f2 );
                Year1=$(echo "$alert" | awk '{print $1}' | cut -d '/' -f3 );
                Time1=$(echo "$alert" | awk '{print $2}' );
                echo "$alert" | grep -Ehroq "#[P M][^[:space:]]"
                if [ $? -eq 0 ]
                then
                        File=$(echo "$alert" | grep -Ehro "#[P M][^[:space:]]*" | cut -d '#' -f2 );
                else
                        echo -e "-Its not a file failure alert...so writing to Final alerts UNCHANGED " >> $Log
                        echo "$alert" >> $NEWLOGS
                        continue
                fi
                echo "$alert" | grep -Ehro "[^[:space:]]*#[P M][^[:space:]]*" | grep -q "^null#"
                if [ $? -eq 0 ]
                then
                        echo -e "-Its a NULL alert...so writing to Final alerts UNCHANGED " >> $Log
                        echo "$alert" >> $NEWLOGS
                        continue
                else
                        Filenm=$(echo "$alert" | grep -Ehro "[^[:space:]]*#[P M][^[:space:]]*");
                fi
                echo "$Year1.$Month1.$Date1 $Time1" > $SCRIPT_WORK_DIRECTORY/timestamp_$HOST_NAME
                grep -q "`cat $SCRIPT_WORK_DIRECTORY/timestamp_$HOST_NAME`" $SFTTracelog
                if [ $? -eq 0 ]
                then
                       # echo -e " \n " >> $Log
                        echo  -e "- Searching for $File in $SFTTracelog ....." >> $Log
                        ERROR "$File" "$SFTTracelog" "$Filenm" "$Time1"
                        #echo -e " \n " >> $Log
                else
                        #echo -e " \n " >> $Log
                        echo -e "- Searching for $File in $SFTTracelog ....." >> $Log
                        echo -e "- $File is NOT FOUND in $SFTTracelog " >> $Log
                        echo -e "- Now searching in $Last_hour_SFTTracelog ....." >> $Log
                        ERROR "$File" "$Last_hour_SFTTracelog" "$Filenm" "$Time1"
                        #echo -e " \n " >> $Log
                fi
        else
                echo -e "\n" >> $Log
                echo ">> Reading the new alert -- "$alert"" >> $Log
                echo -e "- Failed file is not $AlertsCodeList alert code...so writing to Final alerts UNCHANGED " >> $Log
                echo "$alert" >> $NEWLOGS
        fi
done < $Alertslog
cat $NEWLOGS | sort >> $Final_Alertslog
echo $FILE_SIZE > $LOG_SEARCH_FILE
echo -e "\n" >> $Log
echo -e "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" >> $Log
echo "Script Ended" at `date '+%m/%d/%Y'` `date '+%H:%M:%S'` >> $Log
echo "*********************************************************************************************" >> $Log
awk 'END{print}' $Alertslog > $LAST_ALERT
rm $SCRIPT_WORK_DIRECTORY/`basename $0`_$HOST_NAME-FLAG
if [ -e $ERRORLOGS ]
then
        rm $ERRORLOGS
fi
rm $NEWLOGS
rm $Alertslog
#chmod 777 $Log
