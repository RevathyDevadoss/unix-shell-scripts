# Developed by Harish Avadhutha
#!/bin/sh
SPATH=/amex/g1data/Harish/scripts/False_Alert_Verification

uname=`whoami`
Filename="Final_Alerts.log"
ser1="lppma651d.gso.aexp.com"
ser2="lppma652d.gso.aexp.com"
ser3="lppma653d.gso.aexp.com"
if [ $# != 2 ];then
echo "need 2 parameters like"
echo "sh False_Alert_Verification.sh today 00"
echo "sh False_Alert_Verification.sh yesterday 00"
exit
fi
if [ $1 == 'today' ];then
Filename="Final_Alerts.log"
elif [ $1 == 'yesterday' ];then
Filename=$(echo "Final_Alerts.log`date +'%Y-%d' -d '1 day ago'`");
else
echo "need 2 parameters like"
echo "sh False_Alert_Verification.sh today 00"
echo "sh False_Alert_Verification.sh yesterday 00"
exit
fi
X=`echo $2 | tr -dc  '[:digit:]' `
if [ "$X" != "$2"  -o  "$X" == "" ] ; then
echo "second paramter should be numberic"
exit
fi
if [ $2 -lt 00 -o $2 -gt 24 ];then
echo "need 2 parameters. Example"
echo "sh False_Alert_Verification.sh today 00"
echo "sh False_Alert_Verification.sh yesterday 00"
echo "$2 should be numeric"
exit
fi
#if [ $1 == "late" ];then
#Filename=$(echo "Final_Alerts.log`date +'%Y-%d' -d '1 day ago'`");
#fi
if [ $ser1 != `hostname` ];then
scp -P 446 ${uname}@${ser1}:/logs/sft/$Filename $SPATH/Final_Alerts.log651 2>.hari
else
cp /logs/sft/$Filename $SPATH/Final_Alerts.log651
fi
if [ $ser2 != `hostname` ];then
scp -P 446 ${uname}@${ser2}:/logs/sft/$Filename $SPATH/Final_Alerts.log652 2>.hari
else
cp /logs/sft/$Filename $SPATH/Final_Alerts.log652
fi
if [ $ser3 != `hostname` ];then
scp -P 446 ${uname}@${ser3}:/logs/sft/$Filename $SPATH/Final_Alerts.log653 2>.hari
else
cp /logs/sft/$Filename $SPATH/Final_Alerts.log653
fi
cat $SPATH/Final_Alerts.log651 $SPATH/Final_Alerts.log652 $SPATH/Final_Alerts.log653|sort|uniq > $SPATH/Final_Alerts.log
rm $SPATH/Final_Alerts.log651 $SPATH/Final_Alerts.log652 $SPATH/Final_Alerts.log653
less $SPATH/Final_Alerts.log |grep "#P08"|grep -v "BROC00"|grep -v 'P1100.GNS\|M5SUBM.APCDTRANS'|grep -e "SFT_9001" -e "SFT_9015"|awk -F':' '{print $1,$5}'|awk -F'#' '{print $2,$1}'|awk -v myvar="$2" '$3 >= myvar {print $1}'|sort|uniq > /amex/data/SupportScripts/cdTransCheck/tid.properties
sh /amex/data/SupportScripts/cdTransCheck/cdFalseAlertValidation.sh
rm $SPATH/missedtids 2> /dev/null
touch $SPATH/missedtids
chmod 777 $SPATH/missedtids
if [ -f /amex/data/SupportScripts/cdTransCheck/logs/missed_tids ];then
while read line
do
grep "#$line" $SPATH/Final_Alerts.log|awk '{print $NF}'|awk -F'#' '{print $2,$1}'>> $SPATH/missedtids
done</amex/data/SupportScripts/cdTransCheck/logs/missed_tids
echo "MISSED TID(S):"
cat $SPATH/missedtids|sort -u -k1,1
echo ""
echo
echo "Please check further in cd /amex/data/SupportScripts/cdTransCheck/"
fi
