#!/bin/sh
PATH1=/amex/g1data/Harish/Stranded
rm $PATH1/stfile $PATH1/cdfile $PATH1/stverification1 $PATH1/home $PATH1/stmissed $PATH1/stprocessed 2>/dev/null
touch $PATH1/stfile $PATH1/cdfile $PATH1/stverification1 $PATH1/home $PATH1/stmissed $PATH1/stprocessed 
cat "$PATH1/input" | grep "as2inbound" > $PATH1/as2file
cat $PATH1/input | grep -v "as2inbound"  > $PATH1/home
cat $PATH1/home | while read line
do
if [ "$(echo $line | awk -F'/' '{print $8}')" != "" ]
then
ls -ltr $line >> $PATH1/stfile
else
ls -ltr $line >> $PATH1/cdfile
fi
done

function stver
{
ls -ltr /amex/data/repository/$usersubpath/$userpath/$filename* > /dev/null 2>&1
if [ $? -eq 0 ]; then
ls -ltr /amex/data/repository/$usersubpath/$userpath/$filename* > $PATH1/stverification1
date1=`echo $line | awk '{print $7}'`
hour1=`echo $line | awk '{print $8}' | awk -F':' '{print $1}'`
min1=`echo $line | awk '{print $8}' | awk -F':' '{print $2}'`
size1=`echo $line | awk '{print $5}'`
date3=`echo "$date1+1"|bc`
hour3=`echo "($hour1+1)%24"|bc`
min3=`echo "($min1+1)%60"|bc`

#get the file
cat $PATH1/stverification1 | while read ahari
do
date2=`echo $ahari | awk '{print $7}'`
hour2=`echo $ahari | awk '{print $8}'  | awk -F':' '{print $1}'`
min2=`echo $ahari | awk '{print $8}' | awk -F':' '{print $2}'`
size2=`echo $ahari | awk '{print $5}'`
if [ $date1 -eq $date2 ];then
	if [ $hour1 -eq $hour2 ];then
		if [ $min1 -le $min2 -a $size1 -le $size2 ];then
		echo "`echo $line | awk '{print $9}'` ---> false alert" | tee -a $PATH1/stprocessed
		break
		elif [ $min1 -gt $min2 -a $size1 -eq $size2 ];then
                echo "`echo $line | awk '{print $9}'` ---> false alert" | tee -a $PATH1/stprocessed
                break
		fi
	elif [ $hour3 -eq $hour2 ];then
		if [ $min1 -gt $min2  -a $size1 -le $size2  ];then
		echo "`echo $line | awk '{print $9}'` ---> false alert" | tee -a $PATH1/stprocessed
		break
		fi
	fi
elif [ $date3 -eq $date2 ];then
	if [ $hour3 -eq $hour2 ];then
		if [ $min1 -gt $min2 -a $size1 -le $size2 ];then
		echo "`echo $line | awk '{print $9}'` ---> false alert" | tee -a $PATH1/stprocessed
		break
		fi
	fi
fi
done
file=`echo "$line"  | awk '{print $9}'`
cat $PATH1/stprocessed | grep "$file" > /dev/null 2>&1
if [ $? -ne 0 ];then
echo "`echo $line | awk '{print $9}'`" >> $PATH1/stmissed
fi
else
echo "`echo $line | awk '{print $9}'`" >> $PATH1/stmissed
fi
}

function cdver
{
pass=2
cat cdfile | while read line
do
keypass="none"
uploadcheck /dev/null
if [ $keypass == "false" ];then
echo "rename the file `echo $line | awk '{print $9}'`"
fi
done

}

function uploadcheck
{
checksize1=`echo "$line" | awk '{print $5}'`
sleep $pass
file=`echo "$line" | awk '{print $9}'`
checksize2=`ls -ltr $file | awk '{print $5}'`
sleep $pass
checksize3=`ls -ltr $file | awk '{print $5}'`
if [ $checksize1 -ne $checksize2 -a $checksize1 -ne $checksize3 ];then
echo "`echo $line | awk '{print $9}'` ---> file uploading" | tee -a $1
keypass="true"
continue
elif [ $checksize1 -eq $checksize2 -a $checksize1 -eq $checksize3 ];then
keypass="false"
fi
}

#65536,98304 and 32768,131072, 196608
#st file verification
echo -e "\e[1;37m"*************************************************************"\e[0m"
echo "       Verification Start :`date`"
echo -e "\e[1;37m"*************************************************************"\e[0m"
echo
echo -e "\e[1;31m" ST verification started"\e[0m"
cat $PATH1/stfile |  while read line
do
hari=`echo $line | awk '{print $5}'`
#uploading status checking
pass=2
uploadcheck $PATH1/stprocessed

#Magic Number verification
if [ $hari -eq 65536 -o $hari -eq 98304 -o $hari -eq 32768 -o $hari -eq 131072 -o $hari -eq 196608 ]
then
echo "`echo $line | awk '{print $9}'` ---> false alert Magic Number" | tee -a $PATH1/stprocessed
else
#Repositroy verification
userpath=`echo $line | awk '{print $9}' | awk -F'/' '{print $6}'`
usersubpath=`echo $line | awk '{print $9}' | awk -F'/' '{print $5}'`
filename=`echo $line | awk '{print $9}' | awk -F'/' '{print $8}'`
#st checkfuncion run
stver
#cd check and rename function run
fi
done
#cd check and rename function run
if [ $(cat $PATH1/cdfile | wc -l) -ne 0 ];then
echo
echo  -e "\e[1;31m" CD verification started"\e[0m"
cdver
fi
echo
echo -e "\e[1;37m"*********************************************"\e[0m"
echo -e "\e[1;32m""    Total number of AS2 files     :" `cat $PATH1/as2file 2>/dev/null | wc -l`"\e[0m"
echo -e "\e[1;32m""    Total number of CD files      :" `cat $PATH1/cdfile 2>/dev/null | wc -l `"\e[0m"
echo -e "\e[1;32m""    Total number of ST files      :" `cat $PATH1/stfile 2>/dev/null | wc -l`"\e[0m"
echo -e "\e[1;32m""    Total ST files Processed      :" `cat $PATH1/stprocessed 2>/dev/null | wc -l`"\e[0m"
echo -e "\e[1;31m""    Total ST files need sending   :" `cat $PATH1/stmissed 2>/dev/null | wc -l`"\e[0m"
echo -e "\e[1;37m"*********************************************"\e[0m"

echo

chmod 777 stverification1 stmissed stfile home cdfile as2file stprocessed 2> /dev/null
